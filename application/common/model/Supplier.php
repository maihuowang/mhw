<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/28
 * Time: 11:06
 */

namespace app\common\model;

use think\Db;
use think\Model;

class Supplier extends Model
{
    public function getCreateTimeAttr($value, $data)
    {
        return date('Y-m-d H:i:s', $value);
    }
}