<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/4
 * Time: 16:04
 */

namespace app\common\model;

use Think\Model;

/**
 * 主播model
 * Class TheHost
 * @package app\admin\model
 */
class TheHost extends Model
{
    public static function listWhere($params)
    {
        // 条件初始化
        $where = [];
        // uid
        if (!empty($params['user_id'])) {
            $where[] = ['user_id', '=', intval($params['user_id'])];
        }

        if (!empty($params['id'])) {
            $where[] = ['id', '=', intval($params['id'])];
        }

        // 姓名
        if (!empty($params['true_name'])) {
            $where[] = ['true_name', 'like', '%' . trim($params['true_name']) . '%'];
        }

        // 微信号
        if (!empty($params['wx_code'])) {
            $where[] = ['wx_code', 'like', '%' . trim($params['wx_code']) . '%'];
        }

        // tel_number 电话号
        if (!empty($params['tel_number'])) {
            $where[] = ['tel_number', 'like', '%' . intval($params['tel_number']) . '%'];
        }

        return $where;
    }

    /**
     * 列表
     * @param $params
     * @return bool|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getList($params)
    {
        $where = empty($params['where']) ? [] : $params['where'];
        $field = empty($params['field']) ? '*' : $params['field'];
        $order_by = empty($params['order_by']) ? 'id desc' : trim($params['order_by']);
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 50;
        // 获取管理员列表
        $data = self::where($where)->field($field)->order($order_by)->limit($m, $n)->select();
        if ($data) {
            return $data;
        }
        return false;

    }

    /**
     * 总数
     * @param $where
     * @return int
     */
    public static function total($where)
    {
        return (int)self::where($where)->count();
    }

    public function findOne($params)
    {
        $where = empty($params['where']) ? [] : $params['where'];
        return self::where($where)->find();
    }

    public function getRow()
    {

    }


    /**
     * 添加用户
     * @param $data
     * @return int|string
     */
    public static function userSave($data)
    {
        if (isset($data['uid']) && !empty($data['uid'])) {
            $uid = $data['uid'];
            unset($data['uid']);
            return self::getByUid($uid)->allowField(true)->save($data);
        }
        return self::allowField(true)->save($data);
    }


    public function getCreateTimeAttr($value, $data)
    {
        return !empty($value) ? date('Y-m-d H:i:s', $value) : "";
    }
}
