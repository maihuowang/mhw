<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/17
 * Time: 17:08
 */

namespace app\common\behavior;

use app\common\logic\UserMsgLogic;


class UserMsg
{
    public function run($params)
    {
        (new UserMsgLogic())->addMsg($params);
    }

}