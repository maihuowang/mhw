<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------
namespace app\common\logic;

use think\db;

class SupplierLogic
{
    /**
     * 列表
     */
    public static function lists($get)
    {
        $where = [];
        if (isset($get['keyword']) && $get['keyword']) {
            $where[] = ['s.store_full_name', 'like', '%' . $get['keyword'] . '%'];
        }

        $res = db::name('supplier')->alias('s')
            ->leftjoin('user u','s.user_id = u.id')
            ->field('s.*,u.nickname,u.avatar')
            ->where($where);
            
            
        $count = $res->count();
        $lists = $res->page($get['page'], $get['limit'])->select();
        foreach ($lists as $k => $v){
            $lists[$k]['platform'] =  implode(",", json_decode($v['platform'], true));
        }
        return [
            'count' => $count,
            'lists' => $lists,
        ];
    }

    /**
     * 添加
     */
    public static function add($post)
    {

        $data = [
            'name' => $post['name'],
            'contact' => $post['contact'],
            'platform' => !empty($post['platform']) ? json_encode($post['platform']) : json_encode([]),
            'tel' => $post['tel'],
            'remark' => $post['remark'],
            'address' => $post['address'],
            'create_time' => time(),
        ];

        db::name('supplier')
            ->insert($data);
    }

    /**
     * 编辑
     */
    public static function edit($post)
    {
        $user = db::name('supplier')
                ->where("id", $post['id'])
                ->field("user_id")
                ->find();
        if (isset($post['status']) && $post['status'] == 1) {
                db::name("user")->where("id",$user['user_id'])->update(["user_role" => 2]);           
        }
        if(isset($post['status']) && $post['status'] == 3) {
                db::name("user")->where("id",$user['user_id'])->update(["disable" => 1]);
        }

        if(isset($post['category_list']) && $post['category_list']){
            $post['category'] = Db::name("goods_category")->field("name")->where(["id","in",$post['category_list'],["level" => 1]])->select();
        }
        session("user_info" . $user['user_id'], null);
        return db::name('supplier')->where("id", $post['id'])->update($post);
    }

    /**
     * 信息
     */
    public static function info($id)
    {
        $info = db::name('supplier')
            ->where(['id' => $id])
            ->find();
        return $info;

    }

    public static function supplierInfo($supplier_id){
         $info = db::name('supplier')->alias('s')
            ->leftjoin('user u','u.id=s.user_id')
            ->where(['s.id' => $supplier_id,'status' => 1])
            ->field('s.store_logo,s.desc,u.nickname,s.platform,s.id as supplier_id')
            ->find();
            if($info) return $info;
          
    }

    /**
     * 删除
     */
    public static function del($id)
    {

        $data = [
            'del' => 1,
            'update_time' => time()];
        return Db::name('supplier')->where(['del' => 0, 'id' => $id])->update($data);  //逻辑删除


    }

    /**
     * note 获取所有供应商
     */
    public static function getSupplierList()
    {
        $list = Db::name('supplier')
            ->field('id,store_full_name as name')
            ->where(['status' => 1])
            ->select();
        return $list;
    }

    /**
     * note 商户退回加日志记录
     */
     public static function remarks($data)
    {
        $supplier_remarks = $data['supplier_remarks'];
        $supplier_id = $data['id'];
      
        if (empty($supplier_remarks) ||  empty($supplier_id)) {
            return false;
        }
        $supplier_info = Db::name('supplier')->where('id',$supplier_id)->find();
       
        try {
            $result = Db::name('supplier')->where('id', $supplier_id)->update([
                'status' => 3,
                'update_time' => time()
            ]);
            $result = Db::name('user_log_copy')->insert([
                'type'=> 4,
                'title' =>'成为商户被拒',
                'content'=>$supplier_remarks,
                'to_user' => $supplier_info['user_id'],
                'create_time'=>time(),
                'update_time'=>'',
                'is_read' => 0,
                'unll_id' => $supplier_id,
                'from_id'=> 0,
                'unll_type'=> 3

            ]);
            return $result ? true : false;
        } catch (\Exception $e) {
            return false;
        }
    }
}