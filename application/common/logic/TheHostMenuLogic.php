<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/9/3
 * Time: 14:46
 */

namespace app\common\logic;

use Think\Db;

class TheHostMenuLogic
{
    public static function getHostMenu()
    {
        if (session("host_menu")) {
            return session("host_menu");
        }
        $host_menu = Db::name("the_host_menu")->where("status", 0)->column("id,thehostmenuname");
        session("host_menu", $host_menu);
        return $host_menu;
    }
}