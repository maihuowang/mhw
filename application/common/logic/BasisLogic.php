<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/26
 * Time: 15:48
 */

namespace app\common\logic;

use Think\Db;

class BasisLogic
{
    public static function lists($basis_id = 0,$schedule = 0)
    {
        $where = [
            ["status","=","0"]
        ];
        if(!empty($basis_id) && !empty($schedule)){
            //查出直播间的直播时间端ids
            $idsnum = Db::name("basis_studio_interval")->sum('id');
            
            $basis_studio = Db::name("basis_studio")->where("basis_id",$basis_id)->column('name,status','id'); 

            $live_log = Db::name('live_recording')->where(['schedule'=>$schedule,'basis_id'=>array_keys($basis_studio)])->group('basis_studio_id')->column('sum(basis_studio_interval_id) as num ','basis_studio_id');
            foreach ($basis_studio as $key => &$value) {
                    if(isset($live_log[$key])){
                       if($idsnum == $live_log[$key]) $basis_studio[$key]['is_use'] = 1; 
                       if($idsnum > $live_log[$key])  $basis_studio[$key]['is_use'] = 0;
                       //数据异常 抛出直播间
                       if($idsnum < $live_log[$key])  unsset($basis_studio[$key]);
                    }else{
                      $basis_studio[$key]['is_use'] = 0;  
                    }
                   
                 }     
               
            return array_values($basis_studio);                
           
        }

        if(!empty($basis_id)){
            return Db::name("basis_studio")
                ->where("basis_id",$basis_id)
                ->select(); 
        }
        return Db::name("basis")->where($where)->select();

    }
}