<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/17
 * Time: 17:47
 */

namespace app\common\logic;

use think\Db;

class UserMsgLogic
{
    public function addMsg($param)
    {
        $data = [
            "msg_type" => $param['msg_type'] ?? 0,
            "msg_title" => $param['msg_title'],
            "msg_content" => $param['msg_content'],
            "to_user" => $param['to_user'],
            "create_time" => time()
        ];
        Db::name("user_msg")->insert($data);
    }
}