<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------
namespace app\common\logic;

use think\db;

class TheHostLogic
{
    /**
     * 列表
     */
    public static function lists($get)
    {
        $field = "h.*,u.avatar,u.nickname as u_nickname";
        if (isset($get['action'])) {
            $field = "h.id,h.user_id,h.host_label,h.platform,h.sex,h.fans_num,h.nick_name,h.figure_image,h.douyin_code,h.kuaishou_code,h.tianmao_code,h.desc,h.create_time,u.avatar,u.nickname as u_nickname";
        }
        $where = [];
        if (isset($get['keyword']) && $get['keyword']) {
            $where[] = ['h.true_name', 'like', '%' . $get['keyword'] . '%'];
        }
        if (isset($get['nick_name']) && $get['nick_name']) {
            $where[] = ['h.nick_name', 'like', '%' . $get['nick_name'] . '%'];
            $where[] = ['h.status', '=', 1];
        }
        if (isset($get["type"])) {
            switch ($get["type"]) {
                case 2:
                    $where[] = ['h.status', '=', 0];
                    break;
                case 3:
                    $where[] = ['h.status', '=', 1];
                    break;
                case 4:
                    $where[] = ['h.status', '=', 3];
                    break;
            }
        }
        $get['page'] = isset($get['page']) ? $get['page'] : 1;
        $get['limit'] = isset($get['limit']) ? $get['limit'] : 100;
        $res = db::name('the_host')
            ->alias("h")
            ->leftJoin("user u", "h.user_id=u.id")
            ->field($field)
            ->where($where);

        $count = $res->count();

        $lists = $res->page($get['page'], $get['limit'])->select();

        return [
            'count' => $count,
            'lists' => $lists,
        ];
    }

    /**
     * 添加
     */
    public static function add($post)
    {
        $data = [
            'user_id' => $post['user_id'] ?? "",
            'true_name' => $post['true_name'],
            'platform' => !empty($post['platform']) ? json_encode($post['platform']) : json_encode([]),
            'wx_code' => $post['wx_code'],
            'tel_number' => $post['tel_number'],
            'douyin_code' => $post['douyin_code'] ?? "",
            'kuaishou_code' => $post['kuaishou_code'] ?? "",
            'tianmao_code' => $post['tianmao_code'] ?? "",
            'status' => $post['status'] ?? 0,
            'create_time' => time(),
        ];

        db::name('the_host')
            ->insert($data);
    }

    /**
     * 编辑
     */
    public static function edit($post)
    {
        $data = [
            'true_name' => $post['true_name'],
            'wx_code' => $post['wx_code'],
            'tel_number' => $post['tel_number'],
            'douyin_code' => $post['douyin_code'],
            'kuaishou_code' => $post['kuaishou_code'],
            'tianmao_code' => $post['tianmao_code'],
            'status' => $post['status'],
            'sort' => $post['sort'],
            'update_time' => time(),
        ];
        db::name('the_host')
            ->where(['id' => $post['id']])
            ->update($data);

        switch ($post['status']) {
            case 1:
                db::name('user')
                    ->where(['id' => $post['user_id']])
                    ->update(["disable" => 0]);
                break;
            case 2:
                db::name('user')
                    ->where(['id' => $post['user_id']])
                    ->update(["disable" => 1]);
                break;
        }
    }

    /**
     * 审核
     * @param $post
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @throws db\exception\DbException
     */
    public static function audit($post)
    {
        $data = [
            "status" => $post['status']
        ];

        // 启动事务
        Db::startTrans();
        try {
            db::name('the_host')->where(['id' => $post['id']])->update($data);

            if (isset($post['user_id'])) {
                db::name('user')
                    ->where(['id' => $post['user_id']])
                    ->update(["user_role" => $post['user_role']]);
                session("user_info" . $post['user_id'], null);
                $user = db::name('user')->where(['id' => $post['user_id']])->find();
            }

            // 提交事务
            Db::commit();
            return true;
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return false;
        }
    }

    /**
     * 信息
     */
    public static function info($id)
    {
        $info = db::name('the_host')
            ->where(['id' => $id])
            ->find();
        return $info;

    }

    /**
     * 删除
     */
    public static function del($id)
    {
//        $data = ['update_time' => time()];
        $data = [
            'del' => 1,
            'update_time' => time()];
        return Db::name('the_host')->where(['del' => 0, 'id' => $id])->update($data);  //逻辑删除
    }

    /**
     * note 获取所有供应商
     */
    public static function getHostList()
    {
        $list = Db::name('the_host')
            ->field('id,store_full_name as name')
            ->where(['status' => 1])
            ->select();
        return $list;
    }

    public static function getscheduleList($id, $get)
    {
        $where = [
            ["host_id", "=", $id],
            ["del", "=", 0]
        ];
        // 档期
        if (isset($get['schedule']) && $get['schedule']) {
            $where[] = ['schedule', 'like', '%' . $get['schedule'] . '%'];
        }
        // 品类标签
        if (isset($get['label']) && $get['label']) {
            $where[] = ['label', 'like', '%' . $get['label'] . '%'];
        }
        // 状态 0可预约;1已有预约;2不可预约
        if (isset($get['status'])) {
            $where[] = ['status', '=', $get['status']];
        }
        // 预期播出时长(小时h)
        if (isset($get['live_len']) && $get['live_len']) {
            $where[] = ['live_len', '=', $get['live_len']];
        }
        // 发起类型 0主播发起悬赏;1商家发起预约
        if (isset($get['start_type']) && $get['start_type']) {
            $where[] = ['start_type', '=', $get['start_type']];
        }
        // 直播类型 0 混播；1 专场
        if (isset($get['live_type']) && $get['live_type']) {
            $where[] = ['live_type', '=', $get['live_type']];
        }
        // 是否展示 0是 1否
        if (isset($get['is_show']) && $get['is_show']) {
            $where[] = ['is_show', '=', $get['is_show']];
        }
        // 平台
        if (isset($get['platform']) && $get['platform']) {
            $where[] = ['platform', 'like', '%' . $get['platform'] . '%'];
        }
        // 完播
        if (isset($get['finish_status']) && $get['finish_status']) {
            switch ($get['finish_status']) {
                case 1:
                    $where[] = ["schedule", "<", date("Y-m-d", strtotime("-1 day"))];
                    break;
                case 2:
                    $where[] = ["schedule", ">=", date("Y-m-d", strtotime("-1 day"))];
                    break;
            }
        }

        $res = db::name('schedule')
            ->where($where);

        $count = $res->count();
        $lists = $res->page($get['page'], $get['limit'])->order("create_time desc")->select();
        foreach ($lists as $k => $v) {
            $label = db::name('goods_category')->field("name")->where([["id", "in", $v['label']]])->select();
            foreach ($label as $val) {
                $lists[$k]['label_name'][] = $val['name'];
            }
        }

        return [
            'count' => $count,
            'lists' => $lists,
        ];
    }

    /**
     * 查询分类
     * @return array|mixed|\PDOStatement|string|\think\Collection|Db[]|db\Query[]
     * @throws \think\exception\DbException
     * @throws db\exception\DataNotFoundException
     * @throws db\exception\DbException
     * @throws db\exception\ModelNotFoundException
     */
    public static function getCategory()
    {
        return db::name('goods_category')->field("id,name")->where([["level", "=", 1]])->select();
    }
}