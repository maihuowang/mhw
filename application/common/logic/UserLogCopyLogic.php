<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/20
 * Time: 13:54
 */

namespace app\common\logic;

use Think\Db;

class UserLogCopyLogic
{
    public static function getRemarks($data)
    {
        $where = [
            "unll_id" => $data['id'],
            "unll_type" => $data['type'],
        ];
        return Db::name("user_log_copy")
            ->where($where)
            ->order("id desc")
            ->find();
    }

    public static function remarksAdd($data)
    {
        return Db::name("user_log_copy")->allowEmpty(true)->data($data)->insert();
    }
}