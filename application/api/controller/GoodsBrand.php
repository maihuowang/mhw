<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/28
 * Time: 17:38
 */

namespace app\api\controller;

use app\api\logic\GoodsBrandLogic;

class GoodsBrand extends ApiBase
{
    /**
     * 添加品牌
     */
    public function add()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        $check = $this->validate($post, 'app\api\validate\GoodsBrand.add');
        if (true !== $check) {
            $this->_error($check);
        }
        if ($this->user_info['user_role'] != 2) {
            $this->_error("请先成为商户");
        }
        $post['store_id'] = $this->user_info['role']->id;

        $res = GoodsBrandLogic::add($post);
        if ($res === true) {
            $this->_success('提交成功');
        }
        $this->_error($res);
    }

    /**
     * 修改品牌
     */
    public function edit()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        $check = $this->validate($post, 'app\api\validate\GoodsBrand');
        if (true !== $check) {
            $this->_error($check);
        }
        if ($this->user_info['user_role'] != 2) {
            $this->_error("请先成为商户");
        }
        $post['store_id'] = $this->user_info['role']->id;

        $res = GoodsBrandLogic::edit($post);
        if ($res === true) {
            $this->_success('提交成功');
        }
        $this->_error($res);
    }
}