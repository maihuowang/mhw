<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/26
 * Time: 15:45
 */

namespace app\api\controller;

use app\common\logic\BasisLogic;
use app\admin\logic\BaseStaffingLogic;
use app\api\logic\LiveRecording;

class Basis extends ApiBase
{
    public $like_not_need_login = ['lists'];

    /**
     * 基地列表
     * 传入basis_id查询基地下的直播间
     */
    public function lists()
    {
        //基地id
        $basis_id = !empty($this->request->get("basis_id"))?$this->request->get("basis_id"):'';
        //档期
        $schedule = !empty($this->request->get("schedule"))?date("Y-m-d",$this->request->get("schedule")/1000):'';

        $result = BasisLogic::lists($basis_id,$schedule);
        $this->_success("成功",$result);
    }

    /**
     * 主播配置人员
     */
    public function baseStaffinglists()
    {     
         $this->_success('成功', BaseStaffingLogic::lists());
    }

    /**
     * 直播间场次可用
     */
    public function liveIsUse()
    {     
        $basis_studio_id = !empty($this->request->get("basis_studio_id"))?$this->request->get("basis_studio_id"):'';
        //档期
        $schedule = !empty($this->request->get("schedule"))?date("Y-m-d",$this->request->get("schedule")/1000):'';
       
        $this->_success('成功', LiveRecording::useLiveList($basis_studio_id,$schedule));
    }
}