<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------

namespace app\api\controller;

use app\api\logic\IndexLogic;
use app\common\model\Client_;
use app\common\server\ConfigServer;
use app\common\server\UrlServer;
use app\common\logic\SupplierLogic;
use app\api\logic\GoodsLogic;
use app\common\logic\TheHostLogic;
use think\App;
use Think\Db;

class Index extends ApiBase
{
    public $like_not_need_login = ['lists', 'appInit', 'downLine', 'share', 'config', 'seach', 'team', 'ysxy', 'yhxy', 'test'];

    /**
     * note 首页接口
     * create_time 2020/10/21 19:05
     */
    public function lists()
    {
        $lists = IndexLogic::lists($this->user_id);
        return $this->_success('', $lists);
    }

    /**
     * app下载链接  todo
     */
    public function downLine()
    {
        $get = $this->request->get();
        $check = $this->validate($get, 'app\api\validate\App');
        if (true !== $check) {
            $this->_error($check);
        }
        if (isset($get['client']) && $get['client'] == Client_::ios) {
            $this->_success('', ['line' => ConfigServer::get('app', 'line_ios', '')]);
        } else {
            $this->_success('', ['line' => ConfigServer::get('app', 'line_android', '')]);
        }
    }

    /**
     * app初始化接口
     * 苹果不允许单独只有微信第三方登录
     */
    public function appInit()
    {
        $data = [
            'wechat_login' => ConfigServer::get('app', 'wechat_login', '', 0),//微信登录
            //弹出协议
            'agreement' => ConfigServer::get('app', 'agreement', '', 1)
        ];
        $this->_success('', $data);
    }

    /**
     * Notes: 获取分享信息
     * @author 张无忌(2021/1/20 17:04)
     * @return array|mixed|string
     */
    public function share()
    {
        $client = $this->request->get('client', Client_::mnp, 'intval');
        $config = [];
        switch ($client) {
            case Client_::mnp:
                $config = ConfigServer::get('share', 'mnp', [
                    'mnp_share_title' => 'LikeShop——100%开源免费商用电商系统'
                ]);
                break;
            case Client_::oa:
                $config = ConfigServer::get('share', 'h5', [
                    'h5_share_title' => 'LikeShop——100%开源免费商用电商系统',
                    'h5_share_intro' => 'likeshop已经把小程序、安卓APP、苹果APP都免费开源出来了！',
                    'h5_share_image' => ''
                ]);
                if (empty($config['h5_share_image']) and $config['h5_share_image'] !== '') {
                    $config['h5_share_image'] = UrlServer::getFileUrl($config['h5_share_image']);
                }
                break;
            case Client_::android:
            case Client_::ios:
                $config = ConfigServer::get('share', 'app', [
                    'app_share_title' => 'LikeShop——100%开源免费商用电商系统',
                    'app_share_intro' => 'likeshop已经把小程序、安卓APP、苹果APP都免费开源出来了！',
                    'app_share_image' => ''
                ]);
                if (empty($config['app_share_image']) and $config['app_share_image'] !== '') {
                    $config['app_share_image'] = UrlServer::getFileUrl($config['app_share_image']);
                }
                break;
        }
        return $config;
    }

    /**
     * Notes: 设置
     * @author 段誉(2021/2/25 15:39)
     */
    public function config()
    {
        $config = [
            'register_setting' => ConfigServer::get('register_setting', 'open', 0),//注册设置-是否开启短信验证注册
            'app_wechat_login' => ConfigServer::get('app', 'wechat_login', 0),//APP是否允许微信授权登录
            'shop_login_logo' => UrlServer::getFileUrl(ConfigServer::get('website', 'shop_login_logo')),//移动端登录页logo
            'web_favicon' => UrlServer::getFileUrl(ConfigServer::get('website', 'web_favicon')),//浏览器标签图标
            'name' => ConfigServer::get('website', 'name'),//商城名称
            'copyright_info' => ConfigServer::get('copyright', 'company_name'),//版权信息
            'icp_number' => ConfigServer::get('copyright', 'number'),//ICP备案号
            'icp_link' => ConfigServer::get('copyright', 'link'),//备案号链接
            'app_agreement' => ConfigServer::get('app', 'agreement', 0),//app弹出协议
        ];
        $this->_success('', $config);
    }

    /**
     * Notes: 首页搜索
     * @author 段誉(2021/2/25 15:39)
     */
    public function seach()
    {
        $get = $this->request->get(); //获取get请求

        if (!isset($get['keyword']) || !$get['keyword']) {
            $this->_error("缺少参数");
        }

        $get['page'] = isset($get['page']) ? $get['page'] : 1;
        $get['limit'] = isset($get['limit']) ? $get['limit'] : 4;

        //商品
        $data['goods_list'] = GoodsLogic::getGoodsList(['name' => $get['keyword']], $get['page'], $get['limit']);
        //主播
        $data['the_host'] = TheHostLogic::lists(['nick_name' => $get['keyword'], 'action' => $this->request->action()]);
        //商户
        $data['supplier_list'] = SupplierLogic::lists($get);
        $this->_success('搜索成功', $data);
    }

    /**
     * Notes: 首页基地展示，团队力量
     * @author 段誉(2021/2/25 15:39)
     */
    public function team()
    {
        $data['base'] = [
            //基地
            'team' => 'http://oss.salehom.com/images/uploads/images/20210818094859.png',
            //样品间
            'sample' => 'http://oss.salehom.com/images/uploads/images/20210818100213.jpg',
            //直播间
            'broadcasting' => 'http://oss.salehom.com/images/uploads/images/20210818095830.jpg'
        ];
        $data['team'] = [
            [
                'name' => '刘平平',
                'post' => '运营',
                'url' => 'http://oss.salehom.com/images/uploads/images//20210819/ea94dd992f7aaba29ea9a52f3c1f419e.jpg'
            ],
            [
                'name' => '宋雨欣',
                'post' => '商务',
                'url' => 'http://oss.salehom.com/images/uploads/images//20210819/53b7c80d2c10e19ea0c89747f032bb7d.jpg'
            ],
            [
                'name' => '王家浩',
                'post' => '场控',
                'url' => 'http://oss.salehom.com/images/uploads/images/20210819/c8ef9b30126e86830ca30c5b8cc578e7.jpg'
            ]
        ];
        $this->_success('成功', $data);
    }

    // 隐私协议
    public function ysxy()
    {
        $root = $this->request->server('DOCUMENT_ROOT');
        $ysxy = file_get_contents($root . "/static/common/ysxy.txt");
        return $ysxy;
    }

    //用户协议
    public function yhxy()
    {
        $root = $this->request->server('DOCUMENT_ROOT');
        $ysxy = file_get_contents($root . "/static/common/yhxy.txt");
        return $ysxy;
    }

//    public function test()
//    {
//        $data = '{"data":[{"id":"服饰内衣","cat_name":"服饰内衣","sub_categories":[{"id":"女装","cat_name":"女装","sub_categories":[]},{"id":"男装","cat_name":"男装","sub_categories":[]},{"id":"婴童装/亲子装","cat_name":"婴童装/亲子装","sub_categories":[]},{"id":"内衣/袜子","cat_name":"内衣/袜子","sub_categories":[]},{"id":"家居服","cat_name":"家居服","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"食品饮料","cat_name":"食品饮料","sub_categories":[{"id":"零食/坚果/特产","cat_name":"零食/坚果/特产","sub_categories":[]},{"id":"酒水/饮料","cat_name":"酒水/饮料","sub_categories":[]},{"id":"茶","cat_name":"茶","sub_categories":[]},{"id":"粮油米面/南北干货/调味品","cat_name":"粮油米面/南北干货/调味品","sub_categories":[]},{"id":"乳品/咖啡/冲调","cat_name":"乳品/咖啡/冲调","sub_categories":[]},{"id":"方便速食/速冻食品","cat_name":"方便速食/速冻食品","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"日用百货","cat_name":"日用百货","sub_categories":[{"id":"生活日用","cat_name":"生活日用","sub_categories":[]},{"id":"家庭清洁","cat_name":"家庭清洁","sub_categories":[]},{"id":"口腔护理","cat_name":"口腔护理","sub_categories":[]},{"id":"头发洗护/造型","cat_name":"头发洗护/造型","sub_categories":[]},{"id":"身体清洁/护理","cat_name":"身体清洁/护理","sub_categories":[]},{"id":"收纳整理","cat_name":"收纳整理","sub_categories":[]},{"id":"女性护理","cat_name":"女性护理","sub_categories":[]},{"id":"五金工具","cat_name":"五金工具","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"美妆护肤","cat_name":"美妆护肤","sub_categories":[{"id":"护肤品","cat_name":"护肤品","sub_categories":[]},{"id":"彩妆/香水","cat_name":"彩妆/香水","sub_categories":[]},{"id":"美妆工具/美容仪器","cat_name":"美妆工具/美容仪器","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"珠宝饰品","cat_name":"珠宝饰品","sub_categories":[{"id":"潮流饰品","cat_name":"潮流饰品","sub_categories":[]},{"id":"黄金钻石","cat_name":"黄金钻石","sub_categories":[]},{"id":"翡翠玉石","cat_name":"翡翠玉石","sub_categories":[]},{"id":"珍珠","cat_name":"珍珠","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"母婴用品","cat_name":"母婴用品","sub_categories":[{"id":"婴童用品","cat_name":"婴童用品","sub_categories":[]},{"id":"纸尿裤","cat_name":"纸尿裤","sub_categories":[]},{"id":"奶粉/辅食/零食/营养品","cat_name":"奶粉/辅食/零食/营养品","sub_categories":[]},{"id":"孕产妇用品/营养品","cat_name":"孕产妇用品/营养品","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"生鲜蔬果","cat_name":"生鲜蔬果","sub_categories":[{"id":"水果","cat_name":"水果","sub_categories":[]},{"id":"海鲜水产","cat_name":"海鲜水产","sub_categories":[]},{"id":"蔬菜","cat_name":"蔬菜","sub_categories":[]},{"id":"肉/蛋/禽类","cat_name":"肉/蛋/禽类","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"图书音像","cat_name":"图书音像","sub_categories":[{"id":"儿童读物","cat_name":"儿童读物","sub_categories":[]},{"id":"教材教辅","cat_name":"教材教辅","sub_categories":[]},{"id":"其他书刊","cat_name":"其他书刊","sub_categories":[]},{"id":"育儿百科","cat_name":"育儿百科","sub_categories":[]},{"id":"电子教育","cat_name":"电子教育","sub_categories":[]}]},{"id":"鞋靴箱包","cat_name":"鞋靴箱包","sub_categories":[{"id":"箱包","cat_name":"箱包","sub_categories":[]},{"id":"男鞋","cat_name":"男鞋","sub_categories":[]},{"id":"女鞋","cat_name":"女鞋","sub_categories":[]},{"id":"婴童鞋/亲子鞋","cat_name":"婴童鞋/亲子鞋","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"家居家纺","cat_name":"家居家纺","sub_categories":[{"id":"床上用品","cat_name":"床上用品","sub_categories":[]},{"id":"家居饰品","cat_name":"家居饰品","sub_categories":[]},{"id":"居家布艺","cat_name":"居家布艺","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"鲜花绿植","cat_name":"鲜花绿植","sub_categories":[{"id":"鲜花速递","cat_name":"鲜花速递","sub_categories":[]},{"id":"绿植园艺","cat_name":"绿植园艺","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"运动户外","cat_name":"运动户外","sub_categories":[{"id":"运动鞋","cat_name":"运动鞋","sub_categories":[]},{"id":"运动服","cat_name":"运动服","sub_categories":[]},{"id":"运动/户外/骑行用品","cat_name":"运动/户外/骑行用品","sub_categories":[]},{"id":"运动包/户外包/配件","cat_name":"运动包/户外包/配件","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"钟表配饰","cat_name":"钟表配饰","sub_categories":[{"id":"钟表","cat_name":"钟表","sub_categories":[]},{"id":"眼镜","cat_name":"眼镜","sub_categories":[]},{"id":"配饰","cat_name":"配饰","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"3C数码","cat_name":"3C数码","sub_categories":[{"id":"手机","cat_name":"手机","sub_categories":[]},{"id":"电脑/平板/服务器","cat_name":"电脑/平板/服务器","sub_categories":[]},{"id":"数码配件","cat_name":"数码配件","sub_categories":[]},{"id":"数码相机/单反相机/摄像机","cat_name":"数码相机/单反相机/摄像机","sub_categories":[]},{"id":"智能设备","cat_name":"智能设备","sub_categories":[]},{"id":"影音娱乐","cat_name":"影音娱乐","sub_categories":[]},{"id":"商务办公","cat_name":"商务办公","sub_categories":[]},{"id":"话费/宽带/流量","cat_name":"话费/宽带/流量","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"礼品文创","cat_name":"礼品文创","sub_categories":[{"id":"文具","cat_name":"文具","sub_categories":[]},{"id":"创意礼品","cat_name":"创意礼品","sub_categories":[]},{"id":"婚庆节庆","cat_name":"婚庆节庆","sub_categories":[]},{"id":"书法绘画","cat_name":"书法绘画","sub_categories":[]},{"id":"古董文玩","cat_name":"古董文玩","sub_categories":[]},{"id":"办公耗材","cat_name":"办公耗材","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"家具建材","cat_name":"家具建材","sub_categories":[{"id":"住宅家具","cat_name":"住宅家具","sub_categories":[]},{"id":"灯饰照明","cat_name":"灯饰照明","sub_categories":[]},{"id":"家装材料","cat_name":"家装材料","sub_categories":[]},{"id":"厨房卫浴","cat_name":"厨房卫浴","sub_categories":[]},{"id":"商业/办公家具","cat_name":"商业/办公家具","sub_categories":[]},{"id":"五金建材","cat_name":"五金建材","sub_categories":[]},{"id":"装修设计","cat_name":"装修设计","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]},{"id":"全屋定制","cat_name":"全屋定制","sub_categories":[]}]},{"id":"厨卫家电","cat_name":"厨卫家电","sub_categories":[{"id":"餐饮具","cat_name":"餐饮具","sub_categories":[]},{"id":"厨具","cat_name":"厨具","sub_categories":[]},{"id":"生活电器","cat_name":"生活电器","sub_categories":[]},{"id":"大家电","cat_name":"大家电","sub_categories":[]},{"id":"个护电器","cat_name":"个护电器","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"玩具乐器","cat_name":"玩具乐器","sub_categories":[{"id":"玩具/童车/益智/积木/模型","cat_name":"玩具/童车/益智/积木/模型","sub_categories":[]},{"id":"动漫/周边/cos/桌游","cat_name":"动漫/周边/cos/桌游","sub_categories":[]},{"id":"乐器/配件","cat_name":"乐器/配件","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"宠物用品","cat_name":"宠物用品","sub_categories":[{"id":"宠物用品/服饰","cat_name":"宠物用品/服饰","sub_categories":[]},{"id":"宠物食品","cat_name":"宠物食品","sub_categories":[]},{"id":"水族类","cat_name":"水族类","sub_categories":[]},{"id":"宠物保健","cat_name":"宠物保健","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"医药保健","cat_name":"医药保健","sub_categories":[{"id":"滋补品","cat_name":"滋补品","sub_categories":[]},{"id":"护理护具","cat_name":"护理护具","sub_categories":[]},{"id":"医疗器械","cat_name":"医疗器械","sub_categories":[]},{"id":"保健品","cat_name":"保健品","sub_categories":[]},{"id":"医药","cat_name":"医药","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"本地生活","cat_name":"本地生活","sub_categories":[{"id":"旅游度假","cat_name":"旅游度假","sub_categories":[]},{"id":"虚拟卡券","cat_name":"虚拟卡券","sub_categories":[]},{"id":"医疗服务","cat_name":"医疗服务","sub_categories":[]},{"id":"装修服务","cat_name":"装修服务","sub_categories":[]},{"id":"汽车服务","cat_name":"汽车服务","sub_categories":[]},{"id":"其他生活服务","cat_name":"其他生活服务","sub_categories":[]}]},{"id":"汽配摩托","cat_name":"汽配摩托","sub_categories":[{"id":"维修保养/清洗/美容","cat_name":"维修保养/清洗/美容","sub_categories":[]},{"id":"电子导航/车载电器","cat_name":"电子导航/车载电器","sub_categories":[]},{"id":"汽车/摩托车配件","cat_name":"汽车/摩托车配件","sub_categories":[]},{"id":"汽车装饰","cat_name":"汽车装饰","sub_categories":[]},{"id":"汽车/摩托车整车","cat_name":"汽车/摩托车整车","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]},{"id":"其他","cat_name":"其他","sub_categories":[{"id":"机械设备","cat_name":"机械设备","sub_categories":[]},{"id":"农资农具","cat_name":"农资农具","sub_categories":[]},{"id":"其他","cat_name":"其他","sub_categories":[]}]}],"errCode":0}';
//        $data = json_decode($data, true)['data'];
//        foreach ($data as $k => $v) {
//            $level = 1;
//            $pid = 0;
//            $send = [
//                "name" => $v['cat_name'],
//                'level' => $level,
//                "pid" => $pid,
//                "create_time" => time()
//            ];
//            $pid = Db::name("goods_category")->insertGetId($send);
//            if (!empty($v['sub_categories'])) {
//                $level++;
//
//                foreach ($v['sub_categories'] as $key => $val) {
//                    $send = [
//                        "name" => $val['cat_name'],
//                        'level' => $level,
//                        "pid" => $pid,
//                        "create_time" => time()
//                    ];
//                    $pid1 = Db::name("goods_category")->insertGetId($send);
//
//                    if (!empty($val['sub_categories'])) {
//                        $level++;
//
//                        foreach ($val['sub_categories'] as $kee => $vaa) {
//                            $send = [
//                                "name" => $vaa['cat_name'],
//                                'level' => $level,
//                                "pid" => $pid1,
//                                "create_time" => time()
//                            ];
//                            $pid2 = Db::name("goods_category")->insertGetId($send);
//                        }
//                    }
//                }
//            }
//        }
//    }


}