<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:33
 */

namespace app\api\controller;

use app\api\logic\TheHostLogic;
use app\common\logic\TheHostMenuLogic;


class TheHost extends ApiBase
{
    public $like_not_need_login = ['lists', 'thehostmenu', 'hostDetail', 'hostMenu'];

    /**
     * 列表
     */
    public function lists()
    {
        $post = $this->request->post(); //获取请求
        if (!isset($post['page'])) $post['page'] = $this->page_no;
        if (!isset($post['limit'])) $post['limit'] = $this->page_size;
        $this->_success('查询成功', TheHostLogic::lists($post));  //渲染
    }

    /**
     * 详情
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function hostDetail()
    {
        $get = $this->request->get(); //获取get请求
        if (!isset($get['id'])) {
            $this->_error("缺少参数");
        }
        $get['user_id'] = $this->user_id;
        $host = TheHostLogic::hostDetail($get);
        if (!$host) {
            $this->_error("查询失败");
        }
        $this->_success('查询成功', $host);  //渲染
    }

    /**
     * 修改主播信息
     */
    public function edit()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        if (!isset($post['tel_number']) || !isset($post['wx_code'])) {
            $this->_error("缺少参数");
        }
        $res = TheHostLogic::edit($post);
        if (!$res) {
            $this->_error("失败");
        }
        $this->_success("成功");
    }

    /**
     * 档期
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function scheduleList()
    {
        $id = $this->request->get("id");
        $get = $this->request->get();
        if (!$id) {
            $this->_error('缺少参数');
        }
        if (!isset($get['page'])) $get['page'] = $this->page_no;
        if (!isset($get['limit'])) $get['limit'] = $this->page_size;

        $groupDir = TheHostLogic::getscheduleList($id, $get);
        $this->_success('查询成功', $groupDir);
    }

    /**
     * 主播标签
     */
    public function hostMenu()
    {
        $this->_success("成功", TheHostMenuLogic::getHostMenu());
    }

    public function getMyRoleInfo()
    {
        $this->_success("成功",$this->user_info['role']);
    }
}