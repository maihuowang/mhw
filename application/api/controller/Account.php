<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------


namespace app\api\controller;

use app\api\logic\LoginLogic;
use app\api\logic\UserLogic;
use app\api\logic\WechatLogin;
use app\common\logic\UserLogCopyLogic;
use app\common\server\ConfigServer;
use think\facade\Cache;

class Account extends ApiBase
{


    public $like_not_need_login = ['register', 'applogin', 'login', 'mnplogin', 'codeurl', 'oalogin', 'oplogin', 'logout', 'smslogin', 'uinAppLogin', 'authLogin', 'silentLogin', 'getSessionTicket'];


    /**
     * note 注册接口
     * create_time 2020/10/23 18:42
     */
    public function register()
    {
        $post = $this->request->post();

        $post['check_code'] = ConfigServer::get('register_setting', 'open', 1);
        $result = $this->validate($post, 'app\api\validate\Register');
        if ($result === true) {
            $post['romote'] = $this->request->domain(); // 当前域名
            $data = LoginLogic::register($post);
            if ($data) {
                $this->_success('注册成功', $data);
            }
            $this->_error('获取失败', $result, 0);
        }
        $this->_error($result, '', 0);
    }

    /**
     * 成为商户
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function becomeStore()
    {
        $post = $this->request->post();
        $post['create_time'] = time();
        $result = $this->validate($post, 'app\api\validate\BecomeStore');
        if ($result === true) {
            $user_id = $this->user_id;
            $data = LoginLogic::becomeStore($post, $user_id);
            if (is_array($data)) {
                $this->_error($data['msg'], $result, 0);
            }
            $this->_success('申请成功,后台审核中', $data);
        }
        $this->_error($result, '', 0);
    }

    /**
     * 通过ticket查询session中信息
     * @return mixed|null
     */
    public function getSessionTicket()
    {
        $ticket = $this->request->post("ticket");
        if (!$ticket) {
            $this->_error('缺少参数');
        }
        // $session = session($ticket);
        $this->_success("成功", json_decode(Cache::pull($ticket)));
    }

    /**
     * 获取用户详细信息
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getUserInfo()
    {
        $user_id = $this->user_id;

        if (!$user_id) {
            $this->_error('获取失败');
        }
        $result = LoginLogic::getUserInfo($user_id);
        $result['role_status'] = LoginLogic::isRole($user_id);
        if ($result['role_status']['status'] == 3) {
            if ($result['role_status']['role'] == 1) {
                $result['refuse_remark'] = UserLogCopyLogic::getRemarks(["id" => $user_id, "type" => 2]);
            } elseif ($result['role_status']['role'] == 2) {
                $result['refuse_remark'] = UserLogCopyLogic::getRemarks(["id" =>$user_id, "type" => 3]);
            }
        }
        $this->_success("成功", $result);
    }

    /**
     * 成为主播
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function becomeTheHost()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\BecomeTheHost');
        $post['user_id'] = $this->user_id;
        if ($result === true) {
            $data = LoginLogic::becomeTheHost($post);
            if (is_array($data)) {
                $this->_error($data['msg'], $result, 0);
            }
            $this->_success('申请成功,后台审核中', $data);
        }
        $this->_error($result, '', 0);
    }


    /**
     * showdoc
     * @catalog 接口/账号
     * @title 手机号账号登录
     * @description 手机号账号登录
     * @method post
     * @url /account/login
     * @return {"code":1,"msg":"登录成功","data":["token":"3237676fa733d73333341",//登录令牌"nickname":"好象cms-小林",//昵称"avatar":"http://b2c.yixiangonline.com/uploads/user/avatar/3f102df244d5b40f21c4b25dc321c5ab.jpeg",//头像url"level":0,//等级],"show":0,"time":"0.775400"}
     * @param account 必填 string 账号或手机号
     * @param id 必填 int 1-密码登录-2-验证码登录
     * @param password 必填 string 密码
     * @param client 必填 int 客户端类型：2-h5，3-ios，4-android
     * @return_param token string 登录令牌
     * @return_param nickname string 昵称
     * @return_param avatar string 头像
     * @remark
     * @number 1
     */
    public function login()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\api\validate\Login.password');
        if (true !== $check) {
            $this->_error($check);
        }
        $data = LoginLogic::login($post);
        $this->_success('登录成功', $data);
    }


    /**
     * 验证码登录
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function smsLogin()
    {
        $post = $this->request->post();
        $post['message_key'] = 'YZMDL';
        $check = $this->validate($post, 'app\api\validate\Login.code');
        if (true !== $check) {
            $this->_error($check);
        }
        $data = LoginLogic::login($post);
        $this->_success('登录成功', $data);
    }

    //设置用户支付密码
    public function setPayPwd()
    {
        $post = $this->request->post();
        if(!isset($post['pay_PIN']) || !$post['pay_PIN']){
            $this->_error("缺少参数");
        }

        $data = UserLogic::setPayPwd($post,$this->user_id);
        $this->_success('添加成功', $data);
    }


    /**
     * showdoc
     * @catalog 接口/账号
     * @title 小程序登录
     * @description 小程序登录
     * @method post
     * @url /account/mnpLogin
     * @return {"code":1,"msg":"登录成功","data":["token":"3237676fa733d73333341",//登录令牌"nickname":"好象cms-小林",//昵称"avatar":"http://b2c.yixiangonline.com/uploads/user/avatar/3f102df244d5b40f21c4b25dc321c5ab.jpeg",//头像url"level":0,//等级],"show":0,"time":"0.775400"}
     * @param code 必填 string code
     * @param iv 必填 string iv
     * @param encrypted_data 必填 string encrypted_data
     * @return_param token string 登录令牌
     * @return_param nickname string 昵称
     * @return_param avatar string 头像
     * @remark
     * @number 1
     */
    public function mnpLogin()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\api\validate\MnpLogin');
        if (true !== $check) {
            $this->_error($check);
        }
        $data = LoginLogic::mnpLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code']);
    }


    /**
     * showdoc
     * @catalog 接口/账号
     * @title 获取获取向微信请求code的链接
     * @description
     * @method get
     * @url /account/codeurl
     * @param url 必填 varchar 前端当前url
     * @return_param url string codeurl
     * @remark 这里是备注信息
     * @number 0
     * @return  {"code":1,"msg":"获取成功","data":{"url":'http://mp.weixin……'}}
     */
    public function codeUrl()
    {
        $url = $this->request->get('url');
        $this->_success('获取成功', ['url' => LoginLogic::codeUrl($url)], 1);
    }

    /**
     * showdoc
     * @catalog 接口/账号
     * @title 微信H5登录
     * @description 微信H5登录
     * @method post
     * @url /account/oalogin
     * @return {"code":1,"msg":"登录成功","data":["token":"3237676fa733d73333341",//登录令牌"nickname":"好象cms-小林",//昵称"avatar":"http://b2c.yixiangonline.com/uploads/user/avatar/3f102df244d5b40f21c4b25dc321c5ab.jpeg",//头像url"level":0,//等级],"show":0,"time":"0.775400"}
     * @param code 必填 string code
     * @return_param token string 登录令牌
     * @return_param nickname string 昵称
     * @return_param avatar string 头像
     * @remark
     * @number 1
     */
    public function oaLogin()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\api\validate\OaLogin');
        if (true !== $check) {
            $this->_error($check);
        }
        $data = LoginLogic::oaLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code']);
    }

    /**
     * showdoc
     * @catalog 接口/账号
     * @title 微信第三方app登录
     * @description 微信第三方app登录
     * @method post
     * @url /account/oplogin
     * @return {"code":1,"msg":"登录成功","data":["token":"3237676fa733d73333341",//登录令牌"nickname":"好象cms-小林",//昵称"avatar":"http://b2c.yixiangonline.com/uploads/user/avatar/3f102df244d5b40f21c4b25dc321c5ab.jpeg",//头像url"level":0,//等级],"show":0,"time":"0.775400"}
     * @param code 必填 string code
     * @param client 必填 int 客户端类型：3-ios，4-android
     * @return_param token string 登录令牌
     * @return_param nickname string 昵称
     * @return_param avatar string 头像
     * @remark
     * @number 1
     */
    public function opLogin()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\api\validate\OpLogin');
        if (true !== $check) {
            $this->_error($check);
        }
        $data = LoginLogic::opLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code']);
    }


    /**
     * 退出登录
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function logout()
    {
        LoginLogic::logout($this->user_id, $this->client);
        //退出登录只有成功
        $this->_success();
    }


    /**
     * Notes: uniapp微信登录
     * @author 段誉(2021/3/16 16:00)
     */
    public function uinAppLogin()
    {
        $post = $this->request->post();
        $data = LoginLogic::uinAppLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code']);
    }

    //2021-0419  小程序新版登录调整

    /**
     * Notes: 小程序登录(旧系统用户,返回用户信息,否则返回空)
     * @author 段誉(2021/4/19 16:50)
     */
    public function silentLogin()
    {
        $post = $this->request->post();
        if (empty($post['code'])) {
            $this->_error('参数缺失');
        }
        $data = LoginLogic::silentLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code'], $data['show']);
    }


    /**
     * Notes: 小程序登录(新用户登录->需要提交昵称和头像参数)
     */
    public function authLogin()
    {
        $post = $this->request->post();
        if (empty($post['code']) || empty($post['nickname'])) {
            $this->_error('参数缺失');
        }
        $data = LoginLogic::authLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code'], $data['show']);
    }
}