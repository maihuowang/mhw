<?php


namespace app\api\controller;

use app\api\logic\CartLogic;
use app\api\logic\OrderLogic;
use think\Db;

/**
 * 订单
 * Class Order
 * @package app\api\controller
 */
class Order extends ApiBase
{

    public $like_not_need_login = ['goods'];


    /**
     * 订单列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function lists()
    {
        $type = $this->request->get('type', 'all');
        $order_list = OrderLogic::getOrderList($this->user_id, $type, $this->page_no, $this->page_size);
        $this->_success('获取成功', $order_list);
    }


    //下单接口
//    public function buy()
//    {
//        $post = $this->request->post();
//        $post['user_id'] = $this->user_id;
//        $check = $this->validate($post, 'app\api\validate\Order.buy');
//        if (true !== $check) {
//            $this->_error($check);
//        }
//        $action = $post['action'] ?? "";
//
//        $info = OrderLogic::info($post, $this->user_id);
//        if ($info['code'] == 0) {
//            $this->_error($info['msg']);
//        }
//
//        if ($action == 'info') {
//            $this->_success('', $info['data']);
//        }
//
//        $order = OrderLogic::add($this->user_id, $info['data'], $post);
//        return $order;
//    }

    /**
     * 下单接口
     * 2021年9月8日14:19:46
     * @return array|bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function buy()
    {
        $post = $this->request->post();

        if (isset($post['schedule_type']) && $post['schedule_type'] == 1) {
            $check = $this->validate($post, 'app\api\validate\Order.schedule');
        } else {
            $check = $this->validate($post, 'app\api\validate\Order');
        }

        if (true !== $check) {
            $this->_error($check);
        }

        // 购物车
        $cart = CartLogic::info($post, $this->user_info);
        if (!$cart) {
            $this->_error("购物车状态不正确");
        }

        $post['goods_id'] = array_keys($cart);

        // 商品
        $info = OrderLogic::info($post, $this->user_info);
        if ($info['code'] == 0) {
            $this->_error($info['msg']);
        }

        if (empty($info['data'])) {
            $this->_error("商品不存在");
        }

        // 生成订单
        $order = OrderLogic::add($this->user_info, $info['data'], $post, $cart);
        return $order;
    }


    //订单详情
    public function detail()
    {
        $order_id = $this->request->get('id');
        if (!$order_id) {
            $this->_error('请选择订单');
        }
        $order_detail = OrderLogic::getOrderDetail($order_id, $this->user_id);
        if (!$order_detail) {
            $this->_error('订单不存在了!', '');
        }
        $this->_success('获取成功', $order_detail);
    }

//    //取消订单
//    public function cancel()
//    {
//        $order_id = $this->request->post('id');
//        if (empty($order_id)) {
//            $this->_error('参数错误');
//        }
//        return OrderLogic::cancel($order_id, $this->user_id);
//    }

    //取消订单
    public function cancel()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\admin\validate\Order.cancel');
        if (true !== $check) {
            $this->_error($check);
        }
        return OrderLogic::cancel($post['order_id'], $this->user_id, $this->user_info);
    }

    //删除订单
    public function del()
    {
        $order_id = $this->request->post('id');
        if (empty($order_id)) {
            $this->_error('参数错误');
        }
        return OrderLogic::del($order_id, $this->user_id);
    }


    //确认订单
    public function confirm()
    {
        $order_id = $this->request->post('id');
        if (empty($order_id)) {
            $this->_error('参数错误');
        }
        return OrderLogic::confirm($order_id, $this->user_id);
    }


    public function orderTraces()
    {
        $order_id = $this->request->get('id');
        $tips = '参数错误';
        if ($order_id) {
            $traces = OrderLogic::orderTraces($order_id, $this->user_id);
            if ($traces) {
                $this->_success('获取成功', $traces);
            }
            $tips = '暂时物流信息';
        }
        $this->_error($tips);
    }

    /**
     * 审核
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function audit()
    {
        $post = $this->request->post();

        $check = $this->validate($post, 'app\api\validate\OrderAudit');
        if (true !== $check) {
            $this->_error($check);
        }
        $order_id = $this->request->post('order_id');
        $order_status = $this->request->post('order_status');

        $res = OrderLogic::audit($order_id, $order_status, $this->user_info);
        if ($res) {
            $this->_success('审核成功');
        }
        return $this->_error("审核失败", $res);
    }

    /**
     * 押金支付
     * @return array|string
     * @throws \think\Exception
     * @throws \think\db\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function depositPay()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        $post['host_id'] = $this->user_info['role']->id;
        $check = $this->validate($post, 'app\api\validate\OrderPay.deposit');
        if (true !== $check) {
            $this->_error($check);
        }
        return OrderLogic::depositPay($post, $this->user_id);
    }

    public function pay()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        $post['host_id'] = $this->user_info['role']->id;
        $check = $this->validate($post, 'app\api\validate\OrderPay');
        if (true !== $check) {
            $this->_error($check);
        }
        return OrderLogic::pay($post, $this->user_id);
    }

}