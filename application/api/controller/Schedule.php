<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/9
 * Time: 13:03
 */

namespace app\api\controller;

use app\api\logic\ScheduleLogic;

class Schedule extends ApiBase
{
    public $like_not_need_login = [];

    /**
     * 列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function lists()
    {
        $get = $this->request->get();
        $goods_list = ScheduleLogic::getList($get, $get['page_no'] ?? $this->page_no, $get['page_size'] ?? $this->page_size);
        $this->_success('获取成功', $goods_list);
    }

    /**
     * 档期关联订单详情
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getDetail()
    {
        $id = $this->request->get('id');
        $order = ScheduleLogic::getDetailOrder($this->user_id, $id);
        if ($order) {
            $this->_success('获取成功', $order);
        }
        $this->_error('商品不存在', []);
    }

    /**
     * 添加档期
     */
    public function addSchedule()
    {
        $post = $this->request->post();
        $post['user_id'] = $this->user_id;
        $check = $this->validate($post, 'app\api\validate\Schedule');

        if (true !== $check) {
            $this->_error($check);
        }
        $user_id = $this->user_id;
        $result = ScheduleLogic::add($user_id, $post);

        if ($result && $result == 1) {
            $this->_success('添加成功', $result);
        }
        $this->_error("添加失败");
    }


    /**
     * 删除
     */
    public function delSchedule()
    {
        $id = $this->request->post('id');
        if (!$id) {
            $this->_error("缺少参数");

        }
        $result = ScheduleLogic::delSchedule($id);
        if ($result) {
            $this->_success('删除成功', $result);
        }
        $this->_error('删除失败');
    }

    /**
     * 编辑
     */
    public function updateSchedule()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\schedule');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = ScheduleLogic::updateSchedule($user_id, $post);
            if ($result) {
                $this->_success('修改成功', $result);
            }
            $result = '修改失败';
        }
        $this->_error($result);
    }

}