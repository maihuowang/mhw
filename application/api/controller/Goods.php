<?php

namespace app\api\controller;

use Admin\Model\GoodsModel;
use app\api\logic\GoodsCategoryLogic;
use app\api\logic\GoodsLogic;
use app\admin\logic\GoodsLogic as AdminGoodsLogic;
use app\api\server\GainGoodsLink;

class Goods extends ApiBase
{

    public $like_not_need_login = ['getgoodsdetail', 'getgoodslist', 'getbestlist', 'gethostlist', 'getsearchpage', 'getGoodsLike'];

    /**
     * note 商品列表
     * create_time 2020/10/20 11:12
     */
    public function getGoodsList()
    {
        $post = $this->request->post();
        $goods_list = GoodsLogic::getGoodsList($post, $this->page_no, $this->page_size);
        $this->_success('获取成功', $goods_list);
    }

    /**
     * 获取商户的商品列表
     */
    public function getMyGoodsList()
    {
        $get = $this->request->post();

        if ($this->user_info['user_role'] != 2 || empty($this->user_info['role'])) {
            $this->_error("不是商户");
        }
        $supplier_id = $this->user_info['role']->id;

        $goods_list = GoodsLogic::getMyGoodsList($supplier_id, $get, $this->page_no, $this->page_size);
        $this->_success('获取成功', $goods_list);
    }


    /**
     * note 商品详情
     * create_time 2020/10/20 11:12
     */
    public function getGoodsDetail()
    {
        $id = $this->request->get('id');
        $goods = GoodsLogic::getGoodsDetail($this->user_id, $id);
        if ($goods) {
            $this->_success('获取成功', $goods);
        }
        $this->_error('商品不存在', []);
    }

    /**
     * 猜你喜欢
     * @return array|false|mixed|\PDOStatement|string|\think\Collection|\think\Db[]|\think\db\Query[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getGoodsLike()
    {
        $user_id = $this->user_id;
        $result = GoodsLogic::getGoodsLike($user_id);
        $this->_success('获取成功', $result);
    }

    /**
     * note 首页好物优选
     * create_time 2020/10/21 19:04
     */
    public function getBestList()
    {
        $goods_list = GoodsLogic::getBestList($this->page_no, $this->page_size);
        $this->_success('获取成功', $goods_list);
    }

    /**
     * note 热销商品
     * create_time 2020/11/17 9:52
     */
    public function getHostList()
    {
        $goods_list = GoodsLogic::getHostList($this->page_no, $this->page_size);
        $this->_success('获取成功', $goods_list);
    }


    /**
     * note 获取搜索页数据
     * create_time 2020/10/22 16:01
     */
    public function getSearchPage()
    {
        $limit = $this->request->get('limit ', 10);
        $list = GoodsLogic::getSearchPage($this->user_id, $limit);
        $this->_success('', $list);
    }

    /**
     * note 清空搜索记录
     * create_time 2020/12/18 10:26
     */
    public function clearSearch()
    {
        $result = GoodsLogic::clearSearch($this->user_id);
        if ($result) {
            $this->_success('清理成功', '');
        }
        $this->_error('清理失败', '');

    }

    /**
     * 商品添加
     */
    public function goodsAdd()
    {
        $post = $this->request->post();
        $role = $this->user_info['user_role'];
        if ($role != 2) {
            $this->_error('请先成为商户');
        }


        $post['supplier_id'] = $this->user_info['role']->id;

        $result = AdminGoodsLogic::add($post);
        if ($result !== true) {
            $this->_error('添加失败:' . $result);
        }
        $this->_success('添加成功');
    }

    /**
     * 修改商品
     * @throws \think\Exception
     * @throws \think\db\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function goodsEdit()
    {
        $post = $this->request->post();
        $err = 0;
        $suc = 0;
        $count = count($post);
        foreach ($post as $k => $v) {
            if (!$v['id']) {
                $this->_error('缺少参数');
            }
            $role = $this->user_info['user_role'];
            if ($role != 2) {
                $this->_error('请先成为商户');
            }
            if ($v['status'] == 2) {
                $this->_error('商品状态不正确,请下架后再修改商品');
            }
            $v['supplier_id'] = $this->user_info['role']->id;
            $result = AdminGoodsLogic::edit($v);
            if (!$result) {
                $err++;
                continue;
            }
            $suc++;
        }
        $this->_success("共" . $count . "条,提交成功" . $suc . "条,失败" . $err . "条");
    }

    /**
     * 下架
     * @throws \think\Exception
     * @throws \think\db\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function editStatus()
    {
        $post = $this->request->post();
        if (!isset($post['goods_id'])) {
            $this->_error('缺少参数');
        }
        $role = $this->user_info['user_role'];
        if ($role != 2) {
            $this->_error('请先成为商户');
        }
        $post['del'] = 0;
        $status = 0;
        $supplier_id = $this->user_info['role']->id;
        // $goods_id = json_decode($post['goods_id'],true);
        $goods_id = $post['goods_id'];
        foreach ($goods_id as $k => $v) {
            //检测商品是否是上架或者作废商品
            if (!AdminGoodsLogic::testingOrder(['goods_id' => $v, 'status' => $status])) $this->_error('当前商品不可修改');
            $res = GoodsLogic::editStatus($v, $supplier_id, $status);
        }

        $this->_success('商品下架成功');
    }

    /**
     * 作废
     * @throws \think\Exception
     * @throws \think\db\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function delStatus()
    {
        $post = $this->request->post();
        if (!$post['goods_id']) {
            $this->_error('缺少参数');
        }
        $role = $this->user_info['user_role'];
        if ($role != 2) {
            $this->_error('请先成为商户');
        }
        $status = 4;
        $supplier_id = $this->user_info['role']->supplier_id;
        // $goods_id = json_decode($post['goods_id'],true);
        $goods_id = $post['goods_id'];

        foreach ($goods_id as $k => $v) {

            //检测商品是否是上架或者作废商品
            if (!AdminGoodsLogic::testingOrder(['goods_id' => $v, 'status' => $status])) $this->_error('当前商品不可修改');
            $res = GoodsLogic::editStatus($v, $supplier_id, $status);
        }

        $this->_success('商品删除成功');
    }

//    public function goodsLink()
//    {
//        $post = $this->request->post();
//        $this->_success('获取外链成功', GainGoodsLink::getLinkGoodsId($post['link']));
//
//    }
}