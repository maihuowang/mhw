<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\api\logic\UserLogic;
use app\api\logic\GoodsLogic;
use app\api\logic\CartLogic;

class User extends ApiBase
{

    /**
     * note 个人中心
     * create_time 2020/10/23 14:29
     */
    public function center()
    {
        $info = UserLogic::center($this->user_id);
        $this->_success('获取成功', $info);
    }

    /**
     * note 获取用户的基本资料
     * create_time 2020/11/17 15:22
     */
    public function info()
    {
        $this->_success('获取成功', UserLogic::getUserInfo($this->user_id));
    }

    /**
     * note 设置用户的基本资料
     * create_time 2020/11/17 15:22
     */
    public function setInfo()
    {
        $data = $this->request->post();
        $check = $this->validate($data, 'app\api\validate\UpdateUser.set');
        if (true !== $check) {
            $this->_error($check);
        }
        $res = UserLogic::setUserInfo($this->user_id, $data);
        $this->_success('操作成功');
    }

    public function accountLog()
    {
        $source = $this->request->get('source');
        $type = $this->request->get('type', 0);
        $list = [];
        if ($source) {
            $list = UserLogic::accountLog($this->user_id, $source, $type, $this->page_no, $this->page_size);
        }
        $this->_success('获取成功', $list);
    }


    //更换手机号
    public function changeMobile()
    {
        $data = $this->request->post();
        //默认绑定手机号码
        $data['message_key'] = 'BGSJHM';
        $data['client'] = $this->client;
        $validate = 'app\api\validate\ChangeMobile.binding';
        //更换手机号码、替换短信key、验证规则
        if (isset($data['action']) && 'change' == $data['action']) {
            $data['message_key'] = 'BGSJHM';
            $validate = 'app\api\validate\ChangeMobile';
        }
        $data['user_id'] = $this->user_id;
        $check = $this->validate($data, $validate);
        if (true !== $check) {
            $this->_error($check);
        }
        $res = UserLogic::changeMobile($this->user_id, $data);
        if ($res) {
            $this->_success('操作成功');
        }
        $this->_error('操作失败');
    }

    /**
     * 获取用户消息
     */
    public function getUserMsg()
    {
        $user_id = $this->user_id;
        if (!$user_id) {
            $this->_error('缺少参数');
        }
        $res = UserLogic::getUserMsg($user_id);
        if (!$res) {
            $this->_error("失败");
        }
        $this->_success("成功", $res);
    }

    /**
     * 已读
     */
    public function msgIsRead()
    {
        $id = $this->request->get("id");
        $user_id = $this->user_id;
        if (!$user_id) {
            $this->_error('缺少参数');
        }
        $user_id = 5;

        $res = UserLogic::msgIsRead($user_id, $id);
        if (!$res) {
            $this->_error("失败");
        }
        $this->_success("成功");
    }


    //获取微信手机号
    public function getMobile()
    {
        $post = $this->request->post();
        $check = $this->validate($post, 'app\api\validate\WechatMobile');
        if (true !== $check) {
            $this->_error($check);
        }
        return UserLogic::getMobileByMnp($post);
    }


    /**
     * note 我的钱包
     */
    public function myWallet()
    {
        $info = UserLogic::myWallet($this->user_id);
        $this->_success('获取成功', $info);
    }

    /**
     * Notes: 更新微信的用户信息
     * @author 段誉(2021/4/7 15:28)
     */
    public function setWechatInfo()
    {
        $data = $this->request->post();
        $check = $this->validate($data, 'app\api\validate\SetWechatUser');
        if (true !== $check) {
            $this->_error($check);
        }
        $res = UserLogic::updateWechatInfo($this->user_id, $data);
        if (true === $res) {
            $this->_success('操作成功');
        }
        $this->_error('操作失败');
    }

    //获取用户成为达人或商户审核信息
    public function getUsrtExamine()
    {
        $res = UserLogic::getUsrtExamine($this->user_id);
        if ($res) {
            $this->_success('操作成功', $res);
        }
        $this->_error('操作失败');
    }

    //获取用户收藏记录
    public function getUsrtCollection()
    {
        $data = $this->request->get();

        if (!isset($data['type'])) $this->_error('缺少必要参数');
        $res = UserLogic::getUsrtCollection($data['type'], $this->user_id, $this->page_no, $this->page_size);

        if ($res) {
            $this->_success('操作成功', $res);
        }
        $this->_error('操作失败');
    }

    //购物车用户收藏记录添加 购物车数据删除
    public function setUsrtCollection()
    {
        $data = $this->request->post();
        //避免重复插入

        //收藏表商品ids
        $userCollectionList = UserLogic::userCollectionList($this->user_id);

        //前端传值有效商品id
        $goodsIds = GoodsLogic::getGoodsIds($data['collection_id']);
        $newids = [];

        //前端传值有效商品id
        $goodsIds = GoodsLogic::getGoodsIds($data['collection_id']);
        $newids = [];
        foreach ($goodsIds as $key => $value) {
            if (!in_array($value, $userCollectionList)) $newids[] = $value;
        }
        if (count($newids) == 0) $this->_success('操作成功');
        $res = UserLogic::setUsrtCollection($newids, $this->user_id);
        //删除购物车商品
        $del = CartLogic::del($newids, $this->user_id);
        if ($res && $del) {
            $this->_success('操作成功', $res);
        }
        $this->_error('操作失败');
    }

    //商品详情添加到收藏
    public function setGoodsCollection()
    {
        $data = $this->request->post();
        //避免重复插入
        if (UserLogic::getUsrtCollectionInfo($data, $this->user_id)) $this->_success('已有收藏');

        $res = UserLogic::setUsrtCollection([$data['collection_id']], $this->user_id);
        if ($res) {
            $this->_success('操作成功', $res);
        }
        $this->_error('操作失败');
    }

    //用户收藏记录删除
    public function delUsrtCollection()
    {
        $data = $this->request->post();
        if (!isset($data['collection_id']) || empty($data['collection_id'])) $this->_error('缺少必要参数');

        if ($data['type']) $this->_error('缺少必要参数');
        $res = UserLogic::delUsrtCollection($data, $this->user_id);
        if ($res) {
            $this->_success('操作成功', $res);
        }
        $this->_error('操作失败');
    }
}