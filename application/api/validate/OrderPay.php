<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/9/8
 * Time: 17:15
 */

namespace app\api\validate;

use think\Db;
use think\Validate;

class OrderPay extends Validate
{
    protected $rule = [
        'pay_PIN' => 'require|checkPIN',
        'schedule_id' => 'require|checkSchedule|number',
    ];

    protected $message = [
        'pay_PIN.require' => '支付密码必填',
        'schedule_id.require' => '档期ID必填',
    ];

    protected function setDeposit()
    {
        $this->only(['pay_PIN']);
    }

    protected function checkPIN($value, $rule, $data)
    {
        $user_salt = Db::name("user")->field("pay_PIN,salt")->where("id", $data['user_id'])->find();
        if ($user_salt['pay_PIN'] != create_password($value, $user_salt['salt'])) {
            return "密码不正确";
        }
        return true;
    }

    protected function checkSchedule($value, $rule, $data)
    {
        $schedule = Db::name("schedule")->field("schedule_status,id")
            ->where([["id", "=", $value], ["host_id", "=", $data['host_id']], ["del", "=", 0]])
            ->find();
        if (!$schedule) {
            return "档期不存在";
        }

        if ($schedule['schedule_status'] != 0 && $schedule['schedule_status'] != 2) {
            return "档期状态不正确";
        }
        return true;
    }


}

