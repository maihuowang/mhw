<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/9
 * Time: 14:25
 */

namespace app\api\validate;

use think\Db;
use think\Validate;

class Schedule extends Validate
{
    protected $rule = [
        'host_id'    =>'require|number|checkUser',
        'schedule'  =>'require|checkDate',
        'platform'      => 'require',
        'live_type'      => 'require',
        'label'      => 'require',
    ];

    protected $message = [
        'host_id.require'    =>'参数不正确',
        'host_id.number'    =>'参数不正确',
        'schedule.require'  =>'档期不正确',
        'platform.require'      => '平台必填',
        'live_type.require'      => '直播类型必填',
        'label.require'      => '品类标签必填',
    ];

    protected function checkDate($value, $rule, $data)
    {
        try{
            if (strtotime($value) < time()) {
                return "档期格式不正确";
            }
        }catch (\Exception $e){
            return "参数有误";
        }
        return true;
    }
        protected function checkUser($value, $rule, $data)
    {
        $res = Db::name("user")
        ->alias("u")
        ->leftJoin("the_host h", "h.user_id = u.id")
        ->whereor(["u.disable" => 0])
        ->whereor('h.status',1)
        ->find();
        if(!$res){
            return '账号禁用';
        }
        return true;

    }
}