<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------

namespace app\api\validate;

use app\common\logic\SmsLogic;
use app\common\model\Client_;
use think\Db;
use think\Validate;

class UpdateUser extends Validate
{
    protected $rule = [
        'nickname' => 'require|checkName',
        'sex' => 'require',
        'avatar' => 'require',
        'mobile' => 'require',
    ];

    protected $message = [
        'nickname' => '昵称不能为空',
        'sex' => '性别不能为空',
        'avatar' => '头像不能为空',
        'mobile' => '手机号不能为空',
    ];


    public function sceneSet()
    {
        $this->only(['nickname', 'sex', 'avatar']);
    }

    protected function checkField($value, $rule, $data)
    {
        // var_dump($value);die;
        // $allow_field = ['nickname', 'sex', 'avatar', 'mobile'];
        // if (in_array($value, $allow_field)) {
        //     return true;
        // }
        // return '操作失败';
    }

    protected function checkName($value, $rule, $data)
    {
        // iconv_strlen 是统计字符串的字符数量
        if (iconv_strlen($value,"UTF-8") < 2) {
            return '昵称不得小于两位';
        }

        if (iconv_strlen($value,"UTF-8") > 10) {
            return '昵称不得大于十位';
        }
        return true;

    }
}