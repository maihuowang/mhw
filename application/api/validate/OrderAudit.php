<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 13:18
 */

namespace app\api\validate;

use think\Db;
use think\Validate;

class OrderAudit extends Validate
{
    protected $rule = [
        'order_id' => 'require|number',
        'order_status' => 'require|number',
    ];

    protected $message = [
        'order_id.require' => '参数错误',
        'order_id.number' => '参数错误',
        'order_status.require' => '参数缺失',
        'order_status.number' => '参数缺失',
    ];

}

