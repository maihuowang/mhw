<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/30
 * Time: 9:18
 */

namespace app\api\validate;

use think\Db;
use think\Validate;

class GoodsBrand extends Validate
{
    protected $rule = [
        'id' => 'require',
        'name' => 'require|checkNmae',
        'image' => 'require',
        'remark' => 'require',
        'category_id' => 'require'
    ];

    protected $message = [
        'id' => 'ID必填',
        'name.require' => '品牌名称必填',
        'image.require' => '品牌图必填',
        'remark.require' => '简介必填',
        'category_id.require' => '分类必选'
    ];

    //申请售后
    protected function sceneAdd()
    {
        $this->only(['name', 'image', 'remark', 'category_id']);
    }

    protected function checkNmae($value, $rule, $data)
    {
        $check = Db::name("goods_brand")
            ->where([
                ["name","=",$data['name']],
                ["category_id","=",$data['category_id']]
            ])
            ->find();
        if($check){
            return '此品类下的该品牌已存在';
        }
        return true;
    }

}