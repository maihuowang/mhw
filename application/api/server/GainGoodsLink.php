<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/30
 * Time: 15:50
 */

namespace app\api\server;

use app\api\server\CurlServer;

class GainGoodsLink
{
    protected static $linkList = [
        'app.kwaixiaodian.com' => 2,//快手
        'haohuo.jinritemai.com' => 1,//抖音
        'detail.tmall.com' => 3 //天猫
    ];
    protected static $headers = [
        '1' => [
            "Accept-language: zh-cn\r\n" .
            "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; 4399Box.560; .NET4.0C; .NET4.0E)" .
            "Accept: *//*"
        ],//快手
        '2' => [
            "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; 4399Box.560; .NET4.0C; .NET4.0E)",
            "Cookie: _did=web_3114202846467C31; did=web_nvf9m3gp4335a3m69fekkcq8j6w8soy8",
            "Connection: keep-alive",
        ],//抖音
        '3' => [
            "Cookie: _tb_token_=ff6eab377b5ae; cookie2=158de06b8d79511dee53c7458ab4d0ac; t=f4e6014f0ce823a94386ee3dace34aa0"
        ] //天猫
    ];

    public static function getLinkGoodsId($link)
    {
        $linkinfo = parse_url($link);
        $ids = self::findNum($linkinfo['query']);

        foreach (self::$linkList as $key => $value) {
            if (strpos($link, $key)) {
                if ($value == 1) return self::getHaoHuo($ids['id']);
                if ($value == 2) return self::getKwai($ids['id']);
                if ($value == 3) return self::getMao($ids['id']);
            }
        }
    }

    public static function findNum($query = '')
    {
        $queryParts = explode('&', $query);
        $params = array();
        foreach ($queryParts as $param) {
            $item = explode('=', $param);
            $params[$item[0]] = $item[1];
        }
        return $params;

    }

    //抖音
    public static function getHaoHuo($goods_id)
    {

        $url = 'https://ec.snssdk.com/product/fxgajaxstaticitem?id=' . $goods_id;
        $data = CurlServer::send_post($url, 'get', self::$headers['1']);
        $goods_info = [];
        if (!empty($data)) {
            $goods_info['goods_name'] = $data['data']['name'];
            $goods_info['good_price'] = $data['data']['discount_price'] / 100;
            $goods_info['remark'] = '';
            $goods_info['image'] = $data['data']['img'];
            $goods_info['platform'] = 1;
            $goods_info['sales'] = trim($data['data']['sell_num'], '+');
            $goods_info['detailImageUrls'] = $data['data']['img_list'];

        }
        return $goods_info;
    }

    //快手
    public static function getKwai($goods_id)
    {

        $url = 'https://app.kwaixiaodian.com/rest/app/grocery/product/self/detail?itemId=' . $goods_id;
        $data = CurlServer::send_post($url, 'get', self::$headers['2']);
        $goods_info = [];
        if (!empty($data)) {
            $goods_info['goods_name'] = $data['productDetail']['title'];
            $goods_info['good_price'] = $data['productDetail']['price'] / 100;
            $goods_info['remark'] = $data['productDetail']['details'];
            $goods_info['image'] = $data['productDetail']['skuInfoList'][0]['imageUrl'];
            $goods_info['platform'] = 2;
            $goods_info['sales'] = $data['soldNewAmountV2'];
            $goods_info['detailImageUrls'] = $data['productDetail']['detailImageUrls'];

        }
        return $goods_info;
    }

    public static function getMao($goods_id)
    {
        $url = "https://detail.m.tmall.com/item.htm?id=" . $goods_id;

        $data = CurlServer::send_post($url, 'get', self::$headers['3']);
        var_dump($data);
        die;
    }
}