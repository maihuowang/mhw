<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/13
 * Time: 11:05
 */

namespace app\api\server;

class CurlServer
{
   public static function send_post($url, $type = 'get', $header = "", $res = 'json', $arr = '')
{
    $cl = curl_init();
    curl_setopt($cl, CURLOPT_URL, $url);
    curl_setopt($cl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($cl, CURLOPT_SSL_VERIFYHOST, false);
    if ($type == 'post') {
        curl_setopt($cl, CURLOPT_POST, 1);
        curl_setopt($cl, CURLOPT_POSTFIELDS, $arr);
    }
    if (!empty($header)) {
       curl_setopt($cl, CURLOPT_HTTPHEADER, $header);
    }
    $output = curl_exec($cl);
    curl_close($cl);
    return json_decode($output, true);
    if ($res == 'json') {
        if (curl_error($cl)) {
            return curl_error($cl);
        } else {
            return json_decode($output, true);
        }
    }
}
}