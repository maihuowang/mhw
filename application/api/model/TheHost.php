<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/7/30
 * Time: 15:31
 */

namespace app\api\model;

use think\Model;

class TheHost extends Model
{
    public function hostSave($data)
    {
        return $this->allowField(true)->save($data);
    }

    public function Like()
    {
        $host = new self();
        $this->setAttr('like', $host::field("h.id,host_label,goods_label,platform,h.sex,fans_num,h.nick_name,figure_image,host_video,desc,partner_num,u.nickname as user_nickname,u.avatar,h.create_time,h.update_time")
            ->alias("h")
            ->leftJoin("user u","u.id = h.user_id")
            ->limit(8)
            ->select());
    }
}