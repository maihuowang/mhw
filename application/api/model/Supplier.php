<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/1
 * Time: 14:40
 */

namespace app\api\model;

use think\Model;

class Supplier extends Model
{
    public function storeSave($data)
    {
        return $this->allowField(true)->save($data);
    }
}