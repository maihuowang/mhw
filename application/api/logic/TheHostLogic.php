<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:37
 */

namespace app\api\logic;

use app\api\model\TheHost;
use app\common\logic\TheHostMenuLogic;
use think\db;

class TheHostLogic
{
    public static function lists($post)
    {
        $where = [
            ["status", "=", 1]
        ];

        // 模糊搜索
        if (isset($post['keyword']) && $post['keyword']) {
            $where[] = ['nick_name|host_label|goods_label|fans_num|partner_num|desc', 'like', '%' . $post['keyword'] . '%'];
        }
        // 标签
        if (isset($post['host_label']) && $post['host_label']) {
            $where[] = ['host_label', 'like', '%' . $post['host_label'] . '%'];
        }
        // 带货标签 @todo
        if (isset($post['goods_label']) && $post['goods_label']) {
            $where[] = ['goods_label', 'like', '%' . $post['goods_label'] . '%'];
        }
        // 昵称
        if (isset($post['nick_name']) && $post['nick_name']) {
            $where[] = ['nick_name', 'like', '%' . $post['nick_name'] . '%'];
        }
        // 性别
        if (isset($post['sex']) && $post['sex']) {
            $where[] = ['sex', '=', $post['sex']];
        }
        // 平台
        if (isset($post['platform']) && $post['platform']) {
            $where[] = ['platform', 'like', '%' . $post['platform'] . '%'];
        }
        // 粉丝数量
        if (isset($post['start_num']) && $post['start_num']) {
            $where[] = ['fans_num', '>=', stripos($post['start_num'], "w") ? handleRetuenFansNum($post['start_num']) : intval($post['start_num'])];
        }
        if (isset($post['end_num']) && $post['end_num']) {
            $where[] = ['fans_num', '<=', stripos($post['end_num'], "w") ? handleRetuenFansNum($post['end_num']) : intval($post['end_num'])];
        }

        // 档期
        if (isset($post['schedule']) && $post['schedule']) {
            $schedule = Db::name("schedule")->distinct(true)->where("schedule", date("Y-m-d", intval($post['schedule']) / 1000))->column("host_id");
            $where[] = ['id', 'IN', $schedule];
        }
        // 活跃度 @todo
        if (isset($post['active']) && $post['active']) {

        }


        $order = "sort desc";
        // 新加入
        if (isset($post['new_join']) && $post['new_join']) {
            $order = "create_time desc";
        }
        // 合作次数
        if (isset($post['partner_num']) && $post['partner_num']) {
            $order = "partner_num desc";
        }

        $field = "id,user_id,host_label,sex,fans_num,nick_name,figure_image,desc";
        $res = db::name('the_host')->field($field)
            ->where($where);
        $count = $res->count();


        $host_menu = TheHostMenuLogic::getHostMenu();

        $lists = $res
            ->field($field)
            ->page($post['page'], $post['limit'])
            ->order($order)
            ->select();

        foreach ($lists as $k => &$v) {
            $v['label'] = [];
            if ($v['host_label']) {
                $host_label = json_decode($lists[$k]['host_label'], true);
                foreach ($host_label as $key => $val) {
                    $v['label'][] = $host_menu[$val];
                }
            }
            $v['fans_num'] = handleFansNum($v['fans_num']);
        }

        return [
            'count' => $count,
            'lists' => $lists,
            'page' => $post['page'],
            'size' => $post['limit'],
            'more' => is_more($count, $post['page'], $post['limit'])
        ];
    }

    /**
     * 档期
     * @param $id
     * @param $get
     * @return array
     * @throws \think\exception\DbException
     * @throws db\exception\DataNotFoundException
     * @throws db\exception\DbException
     * @throws db\exception\ModelNotFoundException
     */
    public static function getscheduleList($id, $get)
    {
        $where = [
            ["host_id", "=", $id],
            ["del", "=", 0]
        ];
        // 档期
        if (isset($get['schedule']) && $get['schedule']) {
            $where[] = ['schedule', 'like', '%' . $get['schedule'] . '%'];
        }
        // 品类标签
        if (isset($get['label']) && $get['label']) {
            $where[] = ['label', 'like', '%' . $get['label'] . '%'];
        }
        // 状态 0可预约;1已有预约;2不可预约
        if (isset($get['status'])) {
            $where[] = ['status', '=', $get['status']];
        }
        // 预期播出时长(小时h)
        if (isset($get['live_len']) && $get['live_len']) {
            $where[] = ['live_len', '=', $get['live_len']];
        }
        // 发起类型 0主播发起悬赏;1商家发起预约
        if (isset($get['start_type']) && $get['start_type']) {
            $where[] = ['start_type', '=', $get['start_type']];
        }
        // 直播类型 0 混播；1 专场
        if (isset($get['live_type']) && $get['live_type']) {
            $where[] = ['live_type', '=', $get['live_type']];
        }
        // 是否展示 0是 1否
        if (isset($get['is_show']) && $get['is_show']) {
            $where[] = ['is_show', '=', $get['is_show']];
        }
        // 平台
        if (isset($get['platform']) && $get['platform']) {
            $where[] = ['platform', 'like', '%' . $get['platform'] . '%'];
        }
        // 完播
        if (isset($get['finish_status']) && $get['finish_status']) {
            switch ($get['finish_status']) {
                case 1:
                    $where[] = ["schedule", "<", date("Y-m-d", strtotime("-1 day"))];
                    break;
                case 2:
                    $where[] = ["schedule", ">=", date("Y-m-d", strtotime("-1 day"))];
                    break;
            }
        }

        $res = db::name('schedule')
            ->where($where);

        $count = $res->count();
        $lists = $res->page($get['page'], $get['limit'])->order("create_time desc")->select();

        foreach ($lists as $k => $v) {
            $label = db::name('goods_category')->field("name")->where([["id", "in", $v['label']], ["level", "=", 1]])->select();
            foreach ($label as $val) {
                $lists[$k]['label_name'][] = $val['name'];
            }
        }

        return [
            'count' => $count,
            'lists' => $lists,
            'page' => $get['page'],
            'size' => $get['limit'],
            'more' => is_more($count, $get['page'], $get['limit'])
        ];
    }

    /**
     * 详情
     * @param $data
     * @return array|false|mixed|null|\PDOStatement|string|Db|db\Query|\think\Model
     * @throws \think\exception\DbException
     * @throws db\exception\DataNotFoundException
     * @throws db\exception\DbException
     * @throws db\exception\ModelNotFoundException
     */
    public static function hostDetail($data)
    {
        $host = TheHost::field("h.id,host_label,goods_label,platform,h.sex,fans_num,h.nick_name,figure_image,host_video,desc,partner_num,u.nickname as user_nickname,u.avatar,h.create_time,h.update_time")
            ->alias("h")
            ->leftJoin("user u","u.id = h.user_id")
            ->get(['h.id' => $data['id']]);

        if (!$host) {
            return false;
        }
        $host->Like();
        $host_menu = TheHostMenuLogic::getHostMenu();
        if (!empty($host['host_label'])) {
            $host_label = json_decode($host['host_label'], true);
            foreach ($host_label as $v) {
                $label[] = $host_menu[$v];
                $host['label'] = $label;
            }
        }
        if ($data['user_id']) {
            $result = UserLogic::getUsrtCollectionInfo(["collection_id" => $data['id']], $data['user_id'], 1);
            if($result)
            $host['is_collection'] = 1; // 是否被收藏
        }

        $host['platform'] = json_decode($host['platform'], true);
        $host['create_time'] = date("Y-m-d H:i:s", $host['create_time']);
        $host['update_time'] = !empty($host['update_time']) ? date("Y-m-d H:i:s", $host['update_time']) : "";
        $host['record'] = UserLogic::userHigherThanYear(32);
        return $host;
    }

    public static function edit($data)
    {
        // @todo
        return Db::name("the_host")->allowEmpty(true)->where("id", $data['user_id'])->update($data);
    }
}
