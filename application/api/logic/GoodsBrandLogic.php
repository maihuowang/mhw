<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/30
 * Time: 13:41
 */

namespace app\api\logic;

use app\common\Common;
use think\Db;

class GoodsBrandLogic
{
    public static function add($post)
    {
        $initial = GetFirst($post['name']);

        $data = [
            "name" => $post['name'],
            "initial" => $initial,
            "image" => $post['image'],
            "remark" => $post['remark'],
            "category_id" => $post['category_id'],
            "add_type" => 0,
            "add_id" => $post['store_id'],
            "is_show" => 0,
            "status" => 0,
            "create_time" => time(),
        ];
        return Db::name("goods_brand")->insert($data);
    }

    public static function edit($post)
    {
        $initial = GetFirst($post['name']);

        $data = [
            "name" => $post['name'],
            "initial" => $initial,
            "image" => $post['image'],
            "remark" => $post['remark'],
            "category_id" => $post['category_id'],
            "add_type" => 0,
            "add_id" => $post['store_id'],
            "is_show" => 0,
            "status" => 0,
            "create_time" => time(),
        ];
        return Db::name("goods_brand")->where("id",$post['id'])->data($data)->save();
    }
}