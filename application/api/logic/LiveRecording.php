<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/26
 * Time: 15:45
 */

namespace app\api\logic;

use think\Db;

class LiveRecording 
{
    /**
     * 添加直播间使用记录
     */
    public static function add($host_id,$schedule,$basis_id,$basis_studio_id,$basis_studio_interval_id,$status=1)
    {
         $data = [
            'host_id' => $host_id,
            'schedule' => $schedule,
            'basis_id' => $basis_id,
            'basis_studio_id' => $basis_studio_id,
            'basis_studio_interval_id' => $basis_studio_interval_id,
            'status' => $status,
            'create_time' => time(),
        ];
        return  Db::name('live_recording')->insert($data); 
    }

    /**
     * 直播间使用情况
     */
    public static function useLiveList($basis_studio_id = 0, $schedule = 0)
    {     
        if(!empty($basis_studio_id) && !empty($schedule)){  

             $interval_list = Db::name("basis_studio_interval")->select();
             $live_log = Db::name('live_recording')->where(['status'=>1,'basis_studio_id'=>$basis_studio_id,'schedule'=>$schedule])->column('basis_studio_id','basis_studio_interval_id');
         
             foreach ($interval_list as $key => $value) {
                 $interval_list[$key]['is_use'] = 0;
                foreach ($live_log as $k => $v) {
                    
                    if(isset($live_log[$value['id']])) $interval_list[$key]['is_use'] = 1;                    
                }
             }
            
             return $interval_list;

        }
        
    }
}