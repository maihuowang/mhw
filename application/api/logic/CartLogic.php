<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------

namespace app\api\logic;

use app\api\model\Cart;
use app\common\model\Footprint;
use app\common\server\UrlServer;
use think\Db;
use think\facade\Hook;

class CartLogic
{
    //添加购物车
    public static function add($post)
    {
//         $goods = Db::name('goods g')
//             ->field('i.goods_id')
//             ->join('goods_item i', 'i.goods_id = g.id')
//             ->where('i.id', $item_id)
//             ->find();

//         $time = time();
//         $where = [
//             'user_id' => $user_id,
//             'item_id' => $item_id,
//         ];
//         $info = Cart::where($where)->find();

//         $cart_num = $goods_num + (isset($info) ? $info['goods_num'] : 0);

// //        if (self::checkStock($item_id, $cart_num)) {
// //            return '很抱歉,商品库存不足';
// //        }

//         if ($info) {
//             //购物车内已有该商品
//             $update_data = [
//                 'goods_num' => $goods_num + $info['goods_num'],
//                 'update_time' => $time,
//             ];
//             $res = Db::name('cart')
//                 ->where('id', $info['id'])
//                 ->update($update_data);
//         } else {
//             //新增购物车记录
//             $data = [
//                 'user_id' => $user_id,
//                 'goods_id' => $goods['goods_id'],
//                 'goods_num' => $goods_num,
//                 'item_id' => $item_id,
//                 'create_time' => $time,
//             ];
        $data = [
            'user_id' => $post['user_id'],
            'goods_id' => $post['goods_id'],
            'goods_num' => 1,
            'item_id' => 1,
            'create_time' => time(),
        ];
        $res = Db::name('cart')->insert($data);
        // }

        if (!$res) {
            return '添加失败';
        }

//         钩子-记录足迹(添加购物车)
        Hook::listen('footprint', [
            'type' => Footprint::add_cart,
            'user_id' => $post['user_id'],
            'foreign_id' => $post['goods_id'] //商品ID
        ]);

        return true;
    }

    //删除购物车
    public static function del($cart_id, $user_id)
    {
        if (!is_array($cart_id)) return '数据格式错误';

        $where = [
            ['goods_id', 'in', implode(',', $cart_id)],
            ['user_id', '=', $user_id],
        ];

        return Db::name('cart')->where($where)->delete();
    }

    //变动购物车数量
    public static function change($cart_id, $goods_num)
    {
        $cart = Db::name('cart')->where(['id' => $cart_id])->find();

//        if (self::checkStock($cart['item_id'], $goods_num)) {
//            return '很抱歉,库存不足';
//        }

        if ($goods_num <= 0) {
            $goods_num = 1;
        }

        $update = [
            'update_time' => time(),
            'goods_num' => $goods_num,
        ];

        Db::name('cart')->where(['id' => $cart_id])->update($update);

        return true;
    }

    //购物车选中状态
    public static function selected($post, $user_id)
    {
        return Db::name('cart')
            ->where(['id' => $post['cart_id'], 'user_id' => $user_id])
            ->update(['selected' => $post['selected'], 'update_time' => time()]);
    }

    //检查库存
    public static function checkStock($item_id, $goods_num)
    {
        $item_info = Db::name('goods_item')
            ->where('id', $item_id)->find();

        if ($goods_num > $item_info['stock']) {
            return true;
        }
        return false;
    }

    //列表
    public static function lists($user_id, $page_no, $page_size, $selected = '')
    {
        $field = 'g.name,g.image,g.id as goods_id,g.good_price,g.commission,g.coupon,g.status as g_status,g.del as g_del,c.selected,c.id as cart_id,c.item_id,g.supplier_id,s.store_name,g.platform,c.goods_num';
        $where['c.user_id'] = $user_id;
        if ($selected != '') {
            $where['c.selected'] = 1;
        }
        $carts = Db::name('cart c')
            ->field($field)
            ->join('goods g', 'g.id = c.goods_id')
            ->join('supplier s', 's.id = g.supplier_id')
            // ->join('goods_item i', 'i.id = c.item_id')
            ->where($where)
            ->order('c.create_time desc')
            ->page($page_no, $page_size)
            ->group('c.id,g.supplier_id')
            ->select();
        $goods_num = 0;
        $total = 0;

        $lists = [];
        //获取去重过的商户id
        $new_supplier_list = [];
        $supplier_ids = array_unique(array_column($carts, 'supplier_id', 'store_name'));
        foreach ($supplier_ids as $key => $value) {
            $new_supplier_list[$value] = [
                'supplier_id' => $value,
                'store_name' => $key
            ];
        }

        foreach ($carts as $k1 => $cart) {

            $cart['img'] = empty($cart['item_image']) ? UrlServer::getFileUrl($cart['image']) : UrlServer::getFileUrl($cart['item_image']);
            $cart["click"] = 0;
            $cart['cart_status'] = 0;
            if ($cart['g_status'] == 0) {
                $cart['cart_status'] = 1;
            }

            if ($cart['g_del'] == 1) {
                $cart['cart_status'] = 2;
            }

            $cart['selected'] = intval($cart['selected']);
            unset($cart['image'], $cart['item_image']);
            if (isset($new_supplier_list[$cart['supplier_id']])) {
                $new_supplier_list[$cart['supplier_id']]['list'][] = $cart;
            }

        }

        return [
            'lists' => array_values($new_supplier_list),
            'total_amount' => round($total, 2),
            'total_num' => count($carts),
        ];
    }

    //获取购物车数量
    public static function cartNum($user_id)
    {
        $num = Db::name('cart')->alias('c')
            ->join('goods g', 'g.id = c.goods_id')
            ->join('goods_item i', 'i.id = c.item_id')
            ->where('c.user_id', $user_id)
            ->sum('goods_num');
        return ['num' => $num ?? 0];
    }

    /**
     * 购物车详情
     * @param $post
     * @param $user_info
     * @return array
     */
    public static function info($post, $user_info)
    {
        $cart = Db::name('cart')
            ->where([['goods_id', 'in', $post['goods_id']], ["selected", "=", "2"], ["user_id", "=", $user_info['id']]])
            ->column("goods_num",'goods_id');
        return $cart;
    }

}