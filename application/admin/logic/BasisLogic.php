<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:37
 */

namespace app\admin\logic;

use think\db;

class BasisLogic
{
    public static function lists($basis_id='')
    {
      $where[]= ['status','=',0];
      if(!empty($basis_id)){
        $where[] = ['id','=',$basis_id];
      }
       $data_count = Db::name('basis')
            ->where($where)
            ->count();
        $data = Db::name('basis')->where($where)->select();

        return ['list'=>$data,'count' =>$data_count];
    }

       public static function add($post)
    {
       return  Db::name('basis')->insert($post);           
       
    }

        public static function info($id)
    {
       $data = Db::name('basis')
                  ->alias("b")
                  ->leftjoin('basis_role br', 'br.basis_id = b.id')
                  ->where('b.id',$id)
                  ->field("b.*,br.basis_role as role_id")
                  ->find(); 
                  $data['role_id'] = explode(',', $data['role_id']);
                  return $data;          
       
    }

         public static function edit($post)
    {         $basis_role = [
                           'basis_role' =>$post['role_id']
                          ];
              $basis_role_info = Db::name('basis_role')->where('basis_id',$post['id'])->find();
              if($basis_role_info){
                Db::name('basis_role')->where('basis_id',$post['id'])->update($basis_role);    
              }else{
                Db::name('basis_role')->insert(['basis_id'=>$post['id'],'basis_role'=>$post['role_id'],'status'=>0,'create_time'=>time()]);
              }
                       
              
              unset($post['role_id']);
       return  Db::name('basis')->update($post);           
       
    }

          public static function del($id)
    {
       return  Db::name('basis')->where('id',$id)->delete();           
       
    }

    }