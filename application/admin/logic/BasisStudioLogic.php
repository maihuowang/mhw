<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:37
 */

namespace app\admin\logic;

use think\db;

class BasisStudioLogic
{
    public static function lists()
    {
    
       $data_count =  Db::name('basis_studio')
                ->alias('bs')
                ->leftjoin('basis b','b.id=bs.basis_id')
              
                ->where(['b.status'=>0,'bs.status'=>0])->count();
       
        $list = Db::name('basis_studio')
                ->alias('bs')
                ->leftjoin('basis b','b.id=bs.basis_id')
                ->field(['bs.id,b.name as b_name,bs.name,bs.address,bs.status,bs.img'])
                ->where(['b.status'=>0,'bs.status'=>0])->select();
                

         return ['list'=>$list,'count' =>$data_count];
    }

       public static function add($post)
    {
       return  Db::name('basis_studio')->insert($post);           
       
    }

        public static function info($id)
    {
        return  Db::name('basis_studio')
                ->alias('bs')
                ->leftjoin('basis b','b.id=bs.basis_id')
                ->field(['bs.*,b.name as b_name'])
                ->where('bs.id',$id)
                ->where(['b.status'=>0,'bs.status'=>0])
                ->find();
       // return  Db::name('basis')->where('id',$id)->find();           
       
    }

         public static function edit($post)
    {
       return  Db::name('basis_studio')->update($post);           
       
    }

          public static function del($id)
    {
       return  Db::name('basis_studio')->where('id',$id)->delete();           
       
    }

    
}