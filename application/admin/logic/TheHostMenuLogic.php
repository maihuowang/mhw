<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:37
 */

namespace app\admin\logic;

use think\db;

class TheHostMenuLogic
{
    public static function lists()
    {
       $data_count = Db::name('the_host_menu')
            ->where('status',0)
            ->count();
        $data = Db::name('the_host_menu')->where('status',0)->select();

        return ['list'=>$data,'count' =>$data_count];
    }

       public static function add($post)
    {
       return  Db::name('the_host_menu')->insert($post);           
       
    }

        public static function info($id)
    {
       return  Db::name('the_host_menu')->where('id',$id)->find();           
       
    }

         public static function edit($post)
    {
       return  Db::name('the_host_menu')->update($post);           
       
    }

          public static function del($id)
    {
       return  Db::name('the_host_menu')->where('id',$id)->delete();           
       
    }

    
}