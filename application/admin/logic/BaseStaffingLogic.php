<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/11
 * Time: 10:37
 */

namespace app\admin\logic;

use think\db;

class BaseStaffingLogic
{
    public static function lists()
    {
       $data_count = Db::name('base_staffing')
            ->where('status',0)
            ->count();
        $data = Db::name('base_staffing')->where('status',0)->select();

        return ['list'=>$data,'count' =>$data_count];
    }

       public static function add($post)
    {
       return  Db::name('base_staffing')->insert($post);           
       
    }

        public static function info($id)
    {
       return  Db::name('base_staffing')->where('id',$id)->find();           
       
    }

         public static function edit($post)
    {
       return  Db::name('base_staffing')->update($post);           
       
    }

          public static function del($id)
    {
       return  Db::name('base_staffing')->where('id',$id)->delete();           
       
    }

    
}