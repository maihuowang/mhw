<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------

namespace app\admin\logic;

use app\common\logic\OrderGoodsLogic;
use app\common\model\Goods;
use app\common\model\MessageScene_;
use app\common\model\Order;
use app\common\model\OrderLog;
use app\common\logic\OrderLogLogic;
use app\common\server\ConfigServer;
use expressage\Kd100;
use expressage\Kdniao;
use think\Db;
use think\facade\Env;
use think\facade\Hook;

class OrderLogic
{
    // 下一步
    protected static $status = [
        0 => [2, 4, 5],
        1 => [0, 4, 5],
        2 => [3, 4, 5],
        3 => [6],
    ];

    public static function lists($get, $role_id, $role_info)
    {
        $order = new Order();

        $where = [];
        $where[] = ['o.del', '=', 0];

        //订单状态
        if ($get['type'] != '') {
            $where[] = ['order_status', '=', $get['type']];
        }

        //订单搜素
        if (!empty($get['search_key']) && !empty($get['keyword'])) {
            $keyword = $get['keyword'];
            switch ($get['search_key']) {
                case 'order_sn':
                    $where[] = ['o.order_sn', 'like', '%' . $keyword . '%'];
                    break;
                case 'user_sn':
                    $where[] = ['u.sn', 'like', '%' . $keyword . '%'];
                    break;
                case 'nickname':
                    $where[] = ['nickname', 'like', '%' . $keyword . '%'];
                    break;
                case 'user_mobile':
                    $where[] = ['u.mobile', 'like', '%' . $keyword . '%'];
                    break;
                case 'consignee':
                    $where[] = ['consignee', 'like', '%' . $keyword . '%'];
                    break;
                case 'consignee_mobile':
                    $where[] = ['o.mobile', 'like', '%' . $keyword . '%'];
                    break;
            }
        }

        //商品名称
        if (isset($get['goods_name']) && $get['goods_name'] != '') {
            $where[] = ['g.goods_name', 'like', '%' . $get['goods_name'] . '%'];
        }


        //订单类型
        if (isset($get['order_type']) && $get['order_type'] != '') {
            $where[] = ['o.order_type', '=', $get['order_type']];
        }

        //下单时间
        if (isset($get['start_time']) && $get['start_time'] != '') {
            $where[] = ['o.create_time', '>=', strtotime($get['start_time'])];
        }
        if (isset($get['end_time']) && $get['end_time'] != '') {
            $where[] = ['o.create_time', '<=', strtotime($get['end_time'])];
        }

        $field = 'o.*,order_status as order_status_text,u.nickname,u.avatar,u.user_role,h.tel_number as h_tel_number,h.wx_code as host_wx,s.store_name,s.tel_number as s_tel_number,s.wx_code as s_wx';

//        if ($role_id && $role_info) {
//            $where[] = ['o.host_id', '=', $role_info['user_id']]; // 当前用户提交的
//            $where[] = ['o.supplier_id', '=', $role_info['id']]; //  当前用户需要审核的
//        }
        $order->where('schedule', 'not null')->where('schedule', '<', date("Y-m-d", time()))->update(["order_status" => 6]); // 修改当天前的状态为已完成

        $count = $order
            ->alias('o')
            ->field($field)
            ->leftJoin('the_host h', 'h.id = o.host_id')
            ->leftJoin('user u', 'u.id = h.user_id')
            ->leftJoin('supplier s', 's.id = o.supplier_id')
            ->where($where)
            ->group('o.id')
            ->count();

        $lists = $order
            ->alias('o')
            ->field($field)
            ->leftJoin('the_host h', 'h.id = o.host_id')
            ->leftJoin('user u', 'u.id = h.user_id')
            ->leftJoin('supplier s', 's.id = o.supplier_id')
            ->where($where)
            ->page($get['page'], $get['limit'])
            ->order('o.id desc')
            ->group('o.id')
            ->select();

        return ['count' => $count, 'lists' => $lists];
    }

    public static function getDetail($id)
    {
        $order = new Order();

        $res = $order->field("order_type")->where("id", $id)->find();
        if ($res['order_type'] == 1) {  // 商户订单
//            $field = 'a.*,order_status as order_status_text,u.id as user_id,u.nickname,u.avatar,u.user_role,h.true_name,h.tel_number as h_tel_number,h.wx_code as host_wx,h.create_time as h_c_time,s.store_name,s.tel_number as s_tel_number,s.wx_code as s_wx,s.create_time as s_c_time';
//            $result = $order->alias("a")
//                ->leftJoin("the_host h", "h.id = a.host_id")
//                ->leftJoin("supplier s", "s.id = a.supplier_id")
//                ->leftJoin("user u", "h.user_id = u.id")
//                ->leftJoin("supplier ot_su", "ot_su.id = a.supplier_id")
//                ->field("ot_ho.true_name,ot_ho.wx_code as ot_wx,ot_ho.tel_number as ot_tel_number,ot_su.wx_code as ot_wx,ot_su.tel_number as ot_tel_number")
//                ->field($field)
//                ->where('a.id', $id)
//                ->append(['order_status_text', 'order_type_text'])
//                ->find();
            $result = $order->with("order_goods,supplier,the_host")->where('id', $id)->find();
        } elseif ($res['order_type'] == 0) { // 主播订单

//            $field = 'a.*,order_status as order_status_text,u.id as user_id,u.nickname,u.avatar,u.user_role,h.true_name,h.tel_number as h_tel_number,h.wx_code as host_wx,h.create_time as h_c_time,s.store_name,s.tel_number as s_tel_number,s.wx_code as s_wx,s.create_time as s_c_time';
//            $result = $order->alias("a")
//                ->leftJoin("the_host h", "h.id = a.host_id")
//                ->leftJoin("supplier s", "s.id = a.supplier_id")
//                ->leftJoin("user u", "h.user_id = u.id")
//                ->leftJoin("supplier ot_su", "ot_su.id = a.supplier_id")
//                ->field("ot_su.id as ot_id,ot_su.store_name as su_store_name,ot_su.store_url,ot_su.id as ot_supplier_id,ot_su.tel_number as su_tel_number,ot_su.wx_code as su_wx,ot_su.create_time as su_c_time")
//                ->field($field)
//                ->where('a.id', $id)
//                ->append(['order_status_text', 'order_type_text'])
//                ->find();
            $result = $order->with("order_goods,supplier,the_host")->where('id', $id)->find();
        }

        return $result;
    }

    /**
     * 审核
     * @param $order_id
     * @param $order_status
     * @return Order|bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function audit($order_id, $order_status, $admin_info, $role_info)
    {
        $old_status = (new Order())->where("id", $order_id)->field("order_status")->find();
        if (!in_array($order_status, self::$status[$old_status['order_status']])) {
            return false;
        }

        Db::startTrans();
        try {
            $model = (new Order());
            $res = $model->allowField(true)->where("id", $order_id)->data(["order_status" => $order_status])->update();
            if (!$role_info) { // 平台审核
                //订单日志
                OrderLogLogic::record(
                    OrderLog::TYPE_SYSTEM,
                    OrderLog::SYSTEM_CONFIRM_ORDER,
                    $order_id,
                    $admin_info['id'],
                    OrderLog::SYSTEM_CONFIRM_ORDER
                );
            } elseif ($admin_info['user_role'] && $admin_info['user_role'] == 1) { // 主播审核
                OrderLogLogic::record(
                    OrderLog::TYPE_USER,
                    OrderLog::HOST_CONFIRM_ORDER,
                    $order_id,
                    $admin_info['id'],
                    OrderLog::HOST_CONFIRM_ORDER
                );
            } elseif ($admin_info['user_role'] && $admin_info['user_role'] == 2) { // 商户审核
                OrderLogLogic::record(
                    OrderLog::TYPE_SHOP,
                    OrderLog::SHOP_CONFIRM_ORDER,
                    $order_id,
                    $admin_info['id'],
                    OrderLog::SHOP_CONFIRM_ORDER
                );
            }
            Db::commit();
            return $res;
        } catch (\Exception $e) {
            Db::rollback();
            return $e->getMessage();
        }
    }

//    public static function setSchedule($order_id)
//    {
//        $order = (new Order())->where([["id" => $order_id], ["del" => 0]])->find();
//        if (!$order || !$order['schedule']) return false;
//        $schedule = $order['schedule'];
//        $schedule_id = Db::name("schedule")
//            ->field("id")
//            ->where([["id" => $order_id], ["del" => 0], ["status", "!=", 2], ["schedule" => $schedule]])
//            ->find();
//
//        if (!$schedule_id) {
//
//        }
//
//    }

    /**
     * 取消订单
     * @param $order_id
     * @param $admin_info
     * @param $role_info
     */
    public static function cancel($order_id, $admin_info, $role_info)
    {
        $order = Order::get(['del' => 0, 'id' => $order_id], ['order_goods']);

//        OrderGoodsLogic::backStock($order['order_goods'], $order['pay_status']);
        // @todo
        $order->save(['order_status' => Order::STATUS_CLOSE, 'update_time' => time(), 'cancel_time' => time(), 'cancel_user_id' => $admin_info['id']]);

        if (!$role_info) { // 系统取消
            //订单日志
            OrderLogLogic::record(
                OrderLog::TYPE_SYSTEM,
                OrderLog::SYSTEM_CANCEL_ORDER,
                $order_id,
                $admin_info['id'],
                OrderLog::SYSTEM_CANCEL_ORDER
            );
        } elseif ($admin_info['user_role'] && $admin_info['user_role'] == 1) { // 主播取消
            OrderLogLogic::record(
                OrderLog::TYPE_USER,
                OrderLog::HOST_CANCEL_ORDER,
                $order_id,
                $admin_info['id'],
                OrderLog::HOST_CANCEL_ORDER
            );
        } elseif ($admin_info['user_role'] && $admin_info['user_role'] == 2) { // 商户取消
            OrderLogLogic::record(
                OrderLog::TYPE_SHOP,
                OrderLog::SHOP_CANCEL_ORDER,
                $order_id,
                $admin_info['id'],
                OrderLog::SHOP_CANCEL_ORDER
            );
        }
    }

    //删除已取消的订单
    public static function del($order_id, $admin_id)
    {
        $order = Order::get(['del' => 0, 'id' => $order_id]);
        $order->save(['del' => 1, 'update_time' => time()]);

        //订单日志
        OrderLogLogic::record(
            OrderLog::TYPE_SHOP,
            OrderLog::SHOP_DEL_ORDER,
            $order_id,
            $admin_id,
            OrderLog::SHOP_DEL_ORDER
        );
    }

    //物流公司
    public static function express()
    {
        return Db::name('express')->where('del', 0)->select();
    }

    //发货操作
    public static function deliveryHandle($data, $admin_id)
    {
        $order_id = $data['order_id'];
        $order = Order::get(['del' => 0, 'id' => $order_id], ['order_goods']);

        if ($order['shipping_status'] == 1) {
            return true;
        }

        $shipping = Db::name('express')->where('id', $data['shipping_id'])->find();

        //添加发货单
        $delivery_data = [
            'order_id' => $order_id,
            'order_sn' => $order['order_sn'],
            'user_id' => $order['user_id'],
            'admin_id' => $admin_id,
            'consignee' => $order['consignee'],
            'mobile' => $order['mobile'],
            'province' => $order['province'],
            'city' => $order['city'],
            'district' => $order['district'],
            'address' => $order['address'],
            'invoice_no' => $data['invoice_no'],
            'send_type' => $data['send_type'],
            'create_time' => time(),
        ];

        //配送方式->快递配送
        if ($data['send_type'] == 1) {
            $delivery_data['shipping_id'] = $data['shipping_id'];
            $delivery_data['shipping_name'] = $shipping['name'];
            $delivery_data['shipping_status'] = 1;
        }

        $delivery_id = Db::name('delivery')->insertGetId($delivery_data);

        $order->update_time = time();
        $order->shipping_time = time();
        $order->shipping_status = 1;
        $order->order_status = Order::STATUS_WAIT_RECEIVE;
        $order->delivery_id = $delivery_id;
        $order->save();

        //发货短信通知
        if ($order->mobile) {
            $nickname = Db::name('user')->where(['id' => $order['user_id']])->value('nickname');
            $send_data = [
                'key' => 'DDFHTZ',
                'mobile' => $order->mobile,
                'params' => [
                    'order_sn' => $order->order_sn,
                    'nickname' => $nickname,
                ],
            ];
        }


        Hook::listen('sms_send', $send_data);

        // 发货模板消息
        Hook::listen('wx_message_send', [
            'user_id' => $order['user_id'],
            'scene' => MessageScene_::DELIVER_GOODS_SUCCESS,
            'order_id' => $order_id,
            'shipping_name' => $delivery_data['shipping_name'] ?? '无需快递',
            'invoice_no' => $data['invoice_no'] ?? '',
            'delivery_time' => date('Y-m-d H:i:s')
        ]);

        //订单日志
        OrderLogLogic::record(
            OrderLog::TYPE_SHOP,
            OrderLog::SHOP_DELIVERY_ORDER,
            $order_id,
            $admin_id,
            OrderLog::SHOP_DELIVERY_ORDER
        );
    }


    //确认收货
    public static function confirm($order_id, $admin_id)
    {
        $order = Order::get(['del' => 0, 'id' => $order_id]);
        $order->order_status = Order::STATUS_FINISH;
        $order->update_time = time();
        $order->confirm_take_time = time();
        $order->save();

        //订单日志
        OrderLogLogic::record(
            OrderLog::TYPE_SHOP,
            OrderLog::SHOP_CONFIRM_ORDER,
            $order_id,
            $admin_id,
            OrderLog::SHOP_CONFIRM_ORDER
        );
    }

    //物流信息
    public static function shippingInfo($order_id)
    {
        $shipping = Db::name('delivery')->where('order_id', $order_id)->find();
        if ($shipping) {
            $shipping['create_time_text'] = date('Y-m-d H:i:s', $shipping['create_time']);
        }
        $shipping['traces'] = self::getShipping($order_id);
        return $shipping;
    }

    public static function getShipping($order_id)
    {
        $orderModel = new Order();
        $order = $orderModel->alias('o')
            ->field('invoice_no,shipping_name,shipping_id,o.shipping_status')
            ->join('delivery d', 'd.order_id = o.id')
            ->where(['o.id' => $order_id])
            ->find();

        $express = ConfigServer::get('express', 'way', '', '');
        $app = ConfigServer::get($express, 'appkey', '', '');
        $key = ConfigServer::get($express, 'appsecret', '', '');

        if (empty($express) || $order['shipping_status'] != 1 || empty($app) || empty($key)) {
            return $traces[] = ['暂无物流信息'];
        }

        //快递配置设置为快递鸟时
        if ($express === 'kdniao') {
            $expressage = (new Kdniao($app, $key, Env::get('app.app_debug', true)));
            $shipping_field = 'codebird';
        } else {
            $expressage = (new Kd100($key, $app, Env::get('app.app_debug', true)));
            $shipping_field = 'code100';
        }

        //快递编码
        $shipping_code = Db::name('express')
            ->where(['id' => $order['shipping_id']])
            ->value($shipping_field);

        //获取物流轨迹
        $expressage->logistics($shipping_code, $order['invoice_no']);
        $traces = $expressage->logisticsFormat();
        if ($traces == false) {
            $traces[] = ['暂无物流信息'];
        } else {
            foreach ($traces as &$item) {
                $item = array_values(array_unique($item));
            }
        }
        return $traces;
    }


    /**
     * Notes: 订单备注
     * @param $post
     * @param string $type
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author 张无忌(2021/2/1 18:50)
     */
    public static function remarks($post, $type = "get")
    {
        if ($type === 'get') {

            return Db::name('order')->field('id,order_remarks')
                ->where(['id' => $post['id']])
                ->findOrEmpty();
        } else {
            return Db::name('order')
                ->where(['id' => $post['id']])
                ->update(['order_remarks' => $post['order_remarks']]);
        }
    }
}