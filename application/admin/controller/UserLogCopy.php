<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/8/20
 * Time: 13:53
 */

namespace app\admin\controller;

use app\common\logic\UserLogCopyLogic;
use Think\Db;

class UserLogCopy extends AdminBase
{

    public function remarks()
    {
        $post = $this->request->post();
        if (!$post['id'] || !$post['type']) {
            $this->_error("缺少参数");
        }
        return $this->_success("成功", UserLogCopyLogic::getRemarks($post));
    }

    /**
     * 添加
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    // 关联表类型 0订单 1商品 2主播 3商户表
    public function remarksAdd()
    {
        $post = $this->request->post();
        $id = $post['id'];
        $type = $post['type'];
        $table = "";
        $field = "id as info_id,user_id";
        switch ($type) {
            case 0:
                $table = Db::name("order");
                $title = "订单";
                $stat_type = 6;
                break;
            case 1:
                $table = Db::name("goods");
                $title = "商品";
                $stat_type = 5;
                break;
            case 2:
                $table = Db::name("the_host");
                $title = "主播";
                $stat_type = 2;
                break;
            case 3:
                $table = Db::name("supplier");
                $title = "商户";
                $stat_type = 4;
                break;
            case 4:
                $table = Db::name("goods_brand");
                $title = "品牌";
                $stat_type = 8;
                $field = "id as info_id,add_id as user_id";
                break;
        }

        if (empty($table)) {
            $this->_error("缺少参数");
        }

        $info = $table->where("id", $id)->field($field)->find();
        $data = [
            "type" => $stat_type,
            "title" => $title."审核拒绝",
            "content" => $post['content'],
            "to_user" => $info['user_id'],
            "from_id" => $this->admin_id,
            "unll_id" => $id,
            "unll_type" => $type,
            "create_time" => time()
        ];
        $res = UserLogCopyLogic::remarksAdd($data);
        if(!$res){
            $this->_error("失败");
        }
        $this->_success("成功");
    }
}