<?php
// +----------------------------------------------------------------------
// | LikeShop100%开源免费商用电商系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | Gitee下载：https://gitee.com/likeshop_gitee/likeshop
// | 访问官网：https://www.likemarket.net
// | 访问社区：https://home.likemarket.net
// | 访问手册：http://doc.likemarket.net
// | 微信公众号：好象科技
// | 好象科技开发团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------

// | Author: LikeShopTeam
// +----------------------------------------------------------------------
namespace app\admin\validate;

use app\common\model\OrderGoods;
use app\common\server\ConfigServer;
use think\Db;
use think\Validate;

class Supplier extends Validate
{
    protected $rule = [
        'store_full_name' => 'require|checkStoreFullName',
        'store_name' => 'require',
        'store_url' => 'require',
        'wx_code' => 'require|checkWxCode',
        'tel_number' => 'require|mobile|checkMobile',
        // 'license' => 'require',
    ];

    protected $message = [
        'store_full_name.require' => '企业名称必须填写',
        'store_name.require' => '店铺名称必须填写',
        'store_url.require' => '店铺地址必须填写',
        'wx_code.require' => '微信号必须填写',
        'tel_number.require' => '电话号必须填写',
        // 'license.require' => '执照链接必须填写',
        'tel_number.mobile' => '电话号格式不正确'
    ];

    protected function scenetel()
    {
        return $this->only(['tel_number', 'wx_code']);
    }

    public function checkMobile($value, $data, $rule)
    {
        //检查手机号是否已存在
        $user = Db::name('supplier')
            ->where('tel_number', '=', $value)
            ->count();
        if ($user > 1) {
            return '此手机号已被使用';
        }

        return true;
    }

    public function checkWxCode($value, $data, $rule)
    {
        //检查手机号是否已存在
        $user = Db::name('supplier')
            ->where('wx_code', '=', $value)
            ->count();

        if ($user > 1) {
            return '此微信号已被使用';
        }

        return true;
    }

    public function checkStoreFullName($value, $data, $rule)
    {
        //检查企业名称是否已存在
        $user = Db::name('supplier')
            ->where('store_full_name', '=', $value)
            ->count();

        if ($user > 1) {
            return '此企业名称已被使用';
        }

        return true;
    }
}