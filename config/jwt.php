<?php
/**
 * Created by PhpStorm.
 * User: k
 * Date: 2021/7/30
 * Time: 11:34
 */

use think\facade\Env;

return [
    'key' => '123132', // 自定义key值
    'lat' => time(),
    'nbf' => time(),
    'exp' => time() + 24 * 3600 // 过期时间
];