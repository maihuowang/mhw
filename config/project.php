<?php
return [
    'file_domain' => \think\facade\Env::get('project.file_domain'),
    'sms' => true,
    'version'=>'2.3.0.20210326',
    'front_version' => '2.3.0.20210326',
];